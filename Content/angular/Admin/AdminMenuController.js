﻿ angular.module('myApp.controllers')
     .controller('AdminMenuController', ['$state', '$location', '$rootScope', 'settingsAdminService', 'localStorageService', AdminMenuController]);


function AdminMenuController($state, $location, $rootScope, settingsAdminService, localStorageService) {
     var self = this;




    var portal = localStorageService.get('portal');

    if (portal) {
        $rootScope.portal = portal
        $rootScope.portal_id = portal.Id
        $rootScope.portal_name = portal.Name
    }


     self.disabled = function () {
         return self.selected_portal == null
     }

     self.admin_menu_items = [
                              { url: '/#/admin/user', name: 'Users', icon: 'people' },
                              { url: '/#/admin/ruter', name: 'Ruter', icon: 'widgets', disabled: self.disabled },
                             // { url: '/#/register', name: 'Register' },
                              { url: '/#/admin/customer_types', name: 'Customer Types', icon: 'perm_data_setting', disabled: self.disabled },
                              { url: '/#/admin/portal_mapping', name: 'Unittype Mapping', icon: 'layers', disabled: self.disabled },
                              
                              { url: '/#/admin/settings', name: 'Settings', icon: 'settings', disabled: self.disabled },

     ]

    

     // Highligting selected menu button
     self.getClass = function (url) {
         if ('/#' + $location.path().substr(0, url.length - 2) == url || ($location.path() == '/home') && url == '/') {
             return "md-raised md-warn  md-hue-2 icons_left"
         } else {
             return "md-raised md-warn md-hue-1 icons_left"
         }
     }


     self.portals = []
     
     
     settingsAdminService.get_portals()
     .then(function (result) {
         
         self.portals = settingsAdminService.portals;
         if ($rootScope.portal) {
             self.selected_portal =   $.grep(self.portals, function(elm){
                 return elm.Id == $rootScope.portal.Id
             })[0]
         }
         
     })

     self.change_portal = function () {
     
         if (self.selected_portal) {
             
             $rootScope.portal = self.selected_portal
             $rootScope.portal_id = self.selected_portal.Id
             $rootScope.portal_name = self.selected_portal.Name

            
             
             settingsAdminService.set_portal(self.selected_portal)

         } else {
             $rootScope.portal = null
             $rootScope.portal_id = null
             $rootScope.portal_name = null
         }


         
         $rootScope.$broadcast("portal_changed")
         
     }

 }

 