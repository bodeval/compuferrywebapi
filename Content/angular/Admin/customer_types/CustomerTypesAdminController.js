﻿

    angular.module('myApp.controllers')
    .controller('CustomerTypesAdminController', ['customerTypeAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', CustomerTypesAdminController]);

    function CustomerTypesAdminController(customerTypeAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;

        self.portal = $rootScope.portal

        if (!self.portal) {
            $location.path('/admin');
        }


        $rootScope.$on("portal_changed", function () {
            self.portal = $rootScope.portal
            self.get_customer_types()
            
        })

        self.sites = []

        self.customer_types = []



        self.get_customer_types = function () {

            if (self.portal) {


                customerTypeAdminService.getCustomerTypes(self.portal.Id)
                .then(function (result) {
                    console.log(result)
                    self.customer_types = result
                })
            }
        }

        self.get_customer_types()

        customerTypeAdminService.getSites()
        .then(function (result) {
            self.sites = result
        })


        self.show_toast = function (txt) {
            var toast = $mdToast.simple()
               .textContent(txt)
               .position('top right')
               .hideDelay(3000)
               .highlightAction(true)
               .parent("#toast_div");
            //.theme("custom-toast");                

            $mdToast.show(toast)
        }

        self.cancel = function () {
            self.customer_types.shift()
        }

        self.delete = function (type, index) {

            self.customer_types.splice(index, 1);
        
            customerTypeAdminService.delete(type)
          .then(function () {
              self.show_toast("type er slettet !")
          })
        }

        self.add = function () {

            var new_type = { id: 0, "portal_id": self.portal.Id, "company_id": 0, "company_customer_type": "", "portal_customer_type": "" }

            self.customer_types.unshift(new_type)
        }

        self.save = function (type) {
            customerTypeAdminService.save(type)
            .then(function () {
                self.show_toast("type er gemt !")
            })
        }

    }



