﻿
angular.module('myApp.services')
.factory('customerTypeAdminService', ['$http', '$q', CustomerTypeAdminService]);

function CustomerTypeAdminService($http, $q) {

    var server_address = ""
    var self = this;
    var o = {

    };


    o.getSites = function () {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: server_address + '/api/CustomerTypes/GetSites'

            }).success(function (result) {

                return resolve(result);
            });
        });
    }


    o.getCustomerTypes = function (portal_id) {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: server_address + '/api/CustomerTypes/GetCustomerTypes?portal_id=' + portal_id

            }).success(function (result) {

                return resolve(result);
            });
        });
    }

   
    o.save = function (type) {
        return $q(function (resolve, reject) {

            $http({
                method: 'POST',
                url: server_address + '/api/CustomerTypes/Save',
                data: type
            }).success(function (result) {
                return resolve(result);
            });
        });
    }

    o.delete = function (type) {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: server_address + '/api/CustomerTypes/Delete?id=' + type.id 
                
            }).success(function (result) {
                return resolve(result);
            });
        });
    }


    return o;
}
