﻿
angular.module('myApp.services')
.factory('portalMappingAdminService', ['$http', '$q', PortalMappingAdminService]);

function PortalMappingAdminService($http, $q) {

    var token_server_address = ""
    var self = this;
    var o = {

    };

    o.get_portal_sites = function (portal_id) {
        return $q(function (resolve, reject) {            
            $http({
                method: 'GET',
                url: token_server_address + '/api/PortalUnitTypeMapping/GetPortalCompanies?portal_id=' + portal_id
            }).success(function (result) {
                return resolve(result);
            });
        });
    }

  
    o.save = function (site, portal_id) {
        return $q(function (resolve, reject) {

            $http({
                method: 'POST',
                url: token_server_address + '/api/PortalUnitTypeMapping/Save',
                data: { site: site, portal_id: portal_id }

            }).success(function (result) {
                return resolve(result);
            });
        });
    }

    return o;
}
