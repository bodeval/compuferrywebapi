﻿

    angular.module('myApp.controllers')
    .controller('RuterAdminController', ['ruterAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', RuterAdminController]);

    function RuterAdminController(ruterAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location) {
        var self = this;

        console.log(self.portal)
        self.portal = $rootScope.portal

        self.portal_id = $rootScope.portal_id

        

        if (!self.portal) {
            $location.path('/admin');
        }


        $rootScope.$on("portal_changed", function () {
            self.portal = $rootScope.portal
            self.get_routes()
        })

        self.ruter = []
        self.authService = authService;
        self.portal_rutes = []
        self.org_portal_rutes = []
        self.routes_changed = false;


        self.get_routes = function () {
            self.portal_rutes = []
            self.ruter = []

            ruterAdminService.get_portal_rutes(self.portal_id)
            .then(function (portal_rutes) {
                $.each(portal_rutes, function (index, elm) {
                    self.portal_rutes.push(elm.id)
                    self.org_portal_rutes.push(elm.id)
                })
                ruterAdminService.get()
                .then(function (result) {
                    var flag = ""
                    var index = -1
                    var company_routes = []
                    $.each(result, function (i, elm) {
                        if (elm.SelskabsNavn != flag) {
                            index += 1
                            company_routes.push({ name: elm.SelskabsNavn, routes: [] })
                            flag = elm.SelskabsNavn

                        }
                        company_routes[index].routes.push(elm)


                    })

                    self.ruter = company_routes;


                })
            })
        }
        if (self.portal_id) {
            self.get_routes()
        }
        
       

        self.isChecked = function (r) {
            return self.portal_rutes.indexOf(r) > -1
        }

        self.toogle_checked = function (r) {
            //self.routes_changed = true

            
            var index = self.portal_rutes.indexOf(r)
            if (index > -1) {
                self.portal_rutes.splice(index, 1)
            } else {
                self.portal_rutes.push(r)
            }

            self.routes_changed = self.org_portal_rutes.sort(function(a, b){return a> b}).join(",") != self.portal_rutes.sort(function(a, b){return a> b}).join(",")
        }

        self.save = function () {
            
            
            ruterAdminService.save_portal_rutes(self.portal.Id, self.portal_rutes)
            .then(function () {

                self.show_toast("Ruter er gemt")
                
                self.org_portal_rutes = angular.copy(self.portal_rutes)
                self.routes_changed = false;
            })
        }

        self.show_toast = function (txt) {
            var toast = $mdToast.simple()
               .textContent(txt)
               .position('top right')
               .hideDelay(3000)
               .highlightAction(true)
               .parent("#toast_div");
            //.theme("custom-toast");                

            $mdToast.show(toast)
        }

    }



