﻿
angular.module('myApp.services')
.factory('ruterAdminService', ['$http', '$q', RuterAdminService]);

function RuterAdminService($http, $q) {

    var token_server_address = ""
    var self = this;
    var o = {

    };


    o.get = function () {
        return $q(function (resolve, reject) {
            
            $http({
                method: 'GET',
                url: token_server_address + '/api/Routes/get'

            }).success(function (result) {
                
                return resolve(result);
            });
        });
    }

    o.get_portal_rutes = function (portal_id) {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: token_server_address + '/api/Routes/GetPortalRoutes?portal_id=' + portal_id

            }).success(function (result) {
                
                return resolve(result);
            });
        });
    }

    o.save_portal_rutes = function (portal_id, routes) {
        return $q(function (resolve, reject) {

            $http({
                method: 'POST',
                url: token_server_address + '/api/Routes/SaveRoutes',
                data: { routes: routes, portal_id: portal_id }

            }).success(function (result) {

                return resolve(result);
            });
        });
    }
    

    return o;
}
