﻿

angular.module('myApp.controllers')
.controller('SettingsAdminController', ['settingsAdminService', '$scope', '$mdDialog', '$mdToast', '$rootScope', 'authService', '$location', 'mainService', SettingsAdminController]);

function SettingsAdminController(settingsAdminService, $scope, $mdDialog, $mdToast, $rootScope, authService, $location, mainService) {
    var self = this;




   


    self.portal = $rootScope.portal

    if (!self.portal) {
        $location.path('/admin');
    }

    $rootScope.$on("portal_changed", function () {
        self.portal = $rootScope.portal
        self.get_settings()
    })

    self.settings = null

    //self.settings = cs.settings
    //mainService.settings = [{name: "max_number_of_persons", value: 98, type: 'number'},
    //        { name: 'auto_select_date', value: false, type: 'boolean' },
    //        { name: 'auto_open_departures', value: false, type: 'boolean'},
    //        { name: 'show_multible_unittypes', value:  false, type: 'boolean' },
    //        { name: 'show_months_ahead', value: 6, type: 'number' },
    //        { name: 'show_max_number_of_bikes', value: 66, type: 'number' },
    //        { name: 'cykel_statistik_type_id', value: 6 , type: 'boolean' }]
        

    
    self.get_settings = function () {
        if (self.portal) {
            settingsAdminService.get(self.portal.Id)
                .then(function (result) {
                    self.settings = result.settings
                })
        }
        
    }

    self.get_settings()
    
   

    self.edit_editor = function (ev, item) {

        $mdDialog.show({
            locals: { item: item },
            controller: ['$timeout', '$scope', '$mdDialog', '$mdToast', 'item',  EditEditorAdminSettingsController],
            controllerAs: 'ctrl',
            templateUrl: '/content/angular/Admin/settings/_edit_setting.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            preserveScope: true,
            clickOutsideToClose: true,
            
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
            .then(function (setting) {
                item.value = setting
                item = angular.copy(item)
                //$scope.$apply()
                $scope.status = 'Ok';
                $scope.outerForm.$setDirty();   

            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
    }

    self.add = function (ev) {

            $mdDialog.show({
                locals: { info: "" },
                controller: ['$scope', '$mdDialog', '$mdToast', EditAdminSettingsController],
                controllerAs: 'ctrl',
                templateUrl: '/content/angular/Admin/settings/_add_setting.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                preserveScope: true,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
            .then(function (setting) {

                $scope.status = 'Ok';
                self.settings.push(setting)
                
                settingsAdminService.save(self.settings)
                .then(function (result) {                
                    $scope.outerForm.$setPristine();
                    self.show_toast($scope.status)                    
                    mainService.settings = result
                    cs.settings = result                    
                })
                
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
    }

    
    self.save = function () {
        settingsAdminService.save(self.settings, self.portal.Id)
                .then(function (result) {
                    $scope.outerForm.$setPristine();
                    self.show_toast("Settings is saved")
                    mainService.settings = result                 
                    cs.settings = result                    
                })
    }

    self.show_toast = function (txt) {
        var toast = $mdToast.simple()
           .textContent(txt)
           .position('top right')
           .hideDelay(3000)
           .highlightAction(true)
           .parent("#toast_div");
        //.theme("custom-toast");                
        $mdToast.show(toast)
    }
}


function EditAdminSettingsController($scope, $mdDialog, $mdToast) {

    var self = this;

    self.title = "New Setting";
    self.setting_types = ['text', 'number', 'boolean']

    self.setting = {
        name: '',
        description: '',
        type: ''
    }

    self.save = function (setting) {
        $mdDialog.hide(setting);
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };
}


function EditEditorAdminSettingsController($timeout, $scope, $mdDialog, $mdToast, item) {

    var self = this;

    self.title = "Edit Setting";    

    if (item.value) {
        self.value = item.value + ""
    } else{
        self.value = ""
    }
    

    self.codemirrorLoaded = function (_cm) {
        self.value = item.value
     //   alert("loaded")
        _cm.refresh();
        _cm.focus();

    }

    self.save = function (str) {
        item.value = str
        $mdDialog.hide(str);
    }

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };

    $scope.editorOptions = {
        autoRefresh: true,        
        autofocus: true, 
        lineWrapping: false,
        lineNumbers: false,
        matchBrackets: true,
        styleActiveLine: true,
        theme: 'midnight',
        mode: 'javascript',
        json: true,
        firstLineNumber: 1,
        extraKeys: {
            "F11": function (cm) {
                cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            },
            "Esc": function (cm) {
                if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            },
            "Ctrl-S": function (cm) {
                console.log(cm);
                console.log($document[0].activeElement)
            }
        },
        onLoad: function (_cm) {

            // HACK to have the codemirror instance in the scope...
            $scope.modeChanged = function () {
                _cm.setOption("mode", $scope.mode.toLowerCase());
            };

            self.codemirrorLoaded(_cm)
            _cm.setSize(800, '70%')
            _cm.refresh();
            _cm.focus();
            
            
            
        }
    };

    $scope.validateJSON = function (txt) {

        try {
            JSON.parse(txt);
            $scope.myForm.customData.$valid = true;
            // or
            // myForm.customData.$setValidity('valid', true);
        } catch (e) {
            $scope.myForm.customData.$valid = false;
            // or
            // myForm.customData.$setValidity('valid', false);
        }
        return true;
    }



    $scope.refreshCodemirror = true;
    $timeout(function () {
        $scope.refreshCodemirror = false;
    }, 100);

    
}