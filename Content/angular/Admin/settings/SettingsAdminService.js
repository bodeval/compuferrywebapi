﻿
angular.module('myApp.services')
    .factory('settingsAdminService', ['$http', '$q', 'localStorageService', SettingsAdminService]);

function SettingsAdminService($http, $q, localStorageService) {

    var token_server_address = ""
    var self = this;
    var o = {
        portals: []

    };

    o.set_portal = function (portal) {
        localStorageService.set('portal', portal);
        
        
    }

    o.get_portal = function (portal) {
        return localStorageService.get('portal');


    }

    o.get_portals = function () {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: token_server_address + '/api/Settings/GetPortals',

            }).success(function (result) {
                o.portals = result
                return resolve(result);
            });
        });
    }


    o.get = function (portal_id) {
        return $q(function (resolve, reject) {

            $http({
                method: 'GET',
                url: token_server_address + '/api/Settings/Get?portal_id=' + portal_id,

            }).success(function (result) {              
                return resolve(result);
            });
        });
    }



    o.save = function (settings, portal_id) {
        return $q(function (resolve, reject) {

            var user_settings = {}

            $.each(settings, function (index, elm) {

                user_settings[elm.name] = elm.value

            })
            

            var data = {
                system_settings: settings,
                user_settings: user_settings,
                portal_id: portal_id
            }


            $http({
                method: 'POST',
                url: token_server_address + '/api/Settings/Save',
                data: data
            }).success(function (result) {
                return resolve(user_settings);
            });
        });
    }

    return o;
}
