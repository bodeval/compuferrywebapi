﻿angular
      .module('myApp.controllers')
      .controller('HomeController', ['$state', '$location', '$rootScope', '$log', 'hiEventService', 'authService', '$translate', '$scope', 'mainService', 'tourService', '$translate', '$mdDateLocale', 'uiGmapIsReady', '$timeout','$window','$document','$mdToast', '$mdDialog', HomeController]);
function HomeController($state, $location, $rootScope, $log, hiEventService, authService, $translate, $scope, mainService, tourService, $translate, $mdDateLocale, uiGmapIsReady, $timeout, $window, $document, $mdToast, $mdDialog) {
    var self = this    
    var tour = new tourService.tour()

    tourService.start_date = new Date()

    self.language = $rootScope.language;
    $translate.use($rootScope.language);
    
    $scope.$location = $location;
    $scope.$window = $window;
    
    self.is_ready_to_book = tourService.is_ready_to_book;
    $scope.$on('is_ready_to_book', function (event) {
        self.is_ready_to_book = tourService.is_ready_to_book;
    });

    uiGmapIsReady.promise()
     .then(function (map_instances) {
            //  var map1 = $scope.map.control.getGMap();    // get map object through $scope.map.control getGMap() function
            //              var map2 = map_instances[0].map;            // get map object through array object returned by uiGmapIsReady promise
            $rootScope.$broadcast("map_loaded");
     });

    mainService.settings = cs.settings

    

    self.meta_title = mainService.settings.meta_title
    self.settings = mainService.settings
    self.is_persons_selected = mainService.is_persons_selected

    //self.selected_ticket_type
    

    $scope.$on('person_is_selected', function (event) {
        self.is_persons_selected = mainService.is_persons_selected
    });
    
    /* Hvis sat til false bliver kun vist den første enhedstype (cykler)
         * 
         , hvis antal cykler er større end nul bliver denne enhedstype anvendt. */
    self.number_of_persons = function () {
     return mainService.number_of_persons;
    }
        
    self.languages = [{ code: 'da', language: 'Dansk' }, { code: 'en', language: 'English' }] // { code: 'de', language: 'Deutsch' },
    
    self.authentication = authService.authentication
   
    self.change_language = function (language) {
        moment.locale(language)
        $rootScope.language = language
        $translate.use(language);

        $.datepicker.setDefaults($.datepicker.regional[language]);       
        $("#datepicker").datepicker("refresh");        
        var localeDate = moment.localeData();

         $mdDateLocale.shortMonths = localeDate._monthsShort;
         $mdDateLocale.firstDayOfWeek = 1;
         $mdDateLocale.shortDays = localeDate._weekdaysMin;
    }
    
    self.harbors_ready = false
    mainService.get_all()
    .then(function (result) {       
        tourService.tours.push(tour)
        $scope.harbors = result.harbors
        $rootScope.$broadcast("done");
        self.harbors_ready = true        
    })


    self.is_mobile = false

    self.is_mob = function () {        
        if (window.innerWidth <= 800) {                        
            return true;
        } else {
            
            return false;
        }        
    }

    angular.element($window).bind('resize', function () {

        $scope.width = $window.innerWidth;
        self.is_mobile = self.is_mob()
        mainService.is_mobile = self.is_mobile
        $scope.$apply()
    });


    self.tours = tourService.tours

    $scope.$on('formLocator', function (event) {        
        $scope.myDeeplyNestedForm = event.targetScope.myForm;
    });


    self.user_info = null


    $scope.$on('priceUpdated', function (event) {
        self.price_total = tourService.price_total        
    });

    self.admin_book = function (ev) {        
        var user_info = { name: '', email: '', phone: ''}

        $mdDialog.show({
            locals: { user_info: user_info },
            controller: ['$scope', '$mdDialog', '$mdToast', 'user_info', UserInfoController],
            controllerAs: 'ctrl',
            templateUrl: '/content/angular/home/_user_info.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            preserveScope: true,
            clickOutsideToClose: false,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function (answer) {
            self.book(answer)
            
		        $scope.status = 'You said the information was "' + answer + '".';
		    }, function () {
		        self.user_info = null
		    });
    }

    self.book = function (user_info) {
        self.loading = true       
        var book_unit_type = tourService.selected_unit_type

        
        
            
            
        
        mainService.book(book_unit_type, self.tours, user_info)
       .then(function (result) {
           if (result.Success) {

               if (user_info != undefined) {
                   
                   var txt = "Der bliver nu oprettet en booking "
                   var toast = $mdToast.simple()
                  .textContent(txt)
                  .position('bottom right')
                  .hideDelay(3000)
                  .highlightAction(true)
                  .parent("#inc_west_side");
                   

                   $mdToast.show(toast).then(function () {
                       console.log("result.Success")
                       console.log(result.Success)
                       document.location.href = "/"
                   })

                   

               } else {
                   if (result.Uri) {
                       document.location.href = result.Uri
                   }
               }
               
           } else {               
               if (result.MessageList) {
                   self.show_toast(result.MessageList.join(","))
               } else {
                   // if admin booking ok
                   //$location.path('/');
                   document.location.href = "/"
               }
               
               self.loading = false
               self.tours[0].selected_departure = null
               self.is_ready_to_book = false
           }
       })       
    }

    self.show_toast = function (txt) {
        var toast = $mdToast.simple()
           .textContent(txt)
           .position('bottom right')
           .hideDelay(3000)
           .highlightAction(true)
           .parent("#inc_west_side");
        //.theme("custom-toast");                

        $mdToast.show(toast)
    }
}

function UserInfoController($scope, $mdDialog, $mdToast, item) {

    var self = this;
    self.title = "Customer"
    

    //self.item = item;
    self.user_info = item;

    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.answer = function (answer) {
        $mdDialog.hide(answer);
    };


    self.save_user = function (item) {
        $mdDialog.hide(item);
    }
}
