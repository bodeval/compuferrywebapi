﻿
angular
    .module('myApp.controllers')
    .controller('LeftSideNavController', ['$state', '$location', '$rootScope', '$log', 'hiEventService', 'authService', '$translate', 'diverseService', '$scope', 'uiGmapGoogleMapApi', 'mainService', '$filter', 'tourService', '$mdSidenav', '$window','$timeout','$document', LeftSideNavController]);
function LeftSideNavController($state, $location, $rootScope, $log, hiEventService, authService, $translate, diverseService, $scope, uiGmapGoogleMapApi, mainService, $filter, tourService, $mdSidenav, $window, $timeout, $document) {
    var self = this;



   


    $scope.$window = $window;
    
    var t = function (key) {
        return $translate.instant(key)
    }

    self.departures_is_selected = false
   
    var today = new Date()
    self.minDate = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate());

    

    self.maxDate = json2date(cs.settings.maxDate)

    
    
    self.language = $rootScope.language

    self.get_houres = function (item) {        
        return moment(item.DatoTid).hour()
    }

    self.ticket_type_is_enabled = function (item) {
        var result = false
        if (item.type == "nonstop") {
            result = true
        }
        return result
    }

    self.close_selected_ticket_type = function () {
        // clean up
        self.selected_ticket_type = null
        tourService.tours.splice(1)
        self.arrival_date_time = null
        if (self.first_tour.selected_arrival_harbor) {            
            self.close_selected_arrival_harbor()
        } else {
            tourService.is_ready_to_book = false
            $rootScope.$broadcast("is_ready_to_book")
        }
        
        self.departures_is_selected = false
    }
        
    self.close_selected_arrival_harbor = function () {



        self.first_tour.selected_arrival_harbor.label_status = -2
        self.first_tour.selected_arrival_harbor.tour_number = []

        tourService.booking_state = -1

        self.first_tour.select_departure_harbor(self.first_tour.selected_departure_harbor)
        $rootScope.$broadcast("update_map");

        //.update_map_harbors(self.first_tour.selected_arrival_harbor, -2 /* state: departure selected */)

        self.first_tour.selected_arrival_harbor = null
        self.departures_is_selected = false

        // reset book button
        tourService.is_ready_to_book = false
        self.arrival_date_time = null        
        $rootScope.$broadcast("is_ready_to_book")
        
    }


    self.get_last_tour = function () {
        return tourService.get_last_tour()
    }
    
    // change language
    self.get_language = function (item) {        
        try {
            if (item) {                
                return item.Languages[$rootScope.language.toUpperCase()]
            }            
        } 
        catch (e) {            
            console.log(e)            
        }        
    }

    self.datepickerStartDate = null

    self.selected_index = 0
    self.to_day = new Date();
    

    self.t = function (key) {
        return $translate.instant(key)
    }
    

    self.map_loaded = false
    self.is_persons_selected = false

    $rootScope.$on("map_loaded", function () {
        self.map_loaded = true
    })

    self.selected_ticket_type = null
  

    $rootScope.$on("departure_selected", function () {
        self.selected_index = 2
    })


    self.select_departure_harbor = function (item) {        
        tourService.select_departure_harbor(item)
  
    }


    self.prioritized_harbors = mainService.prioritized_harbors

    self.ticket_types = mainService.ticket_types

    self.arrival_date_time = null


    self.select_first_departure = function(item) {
        // only retur
        self.get_last_tour().selected_departure = null
        self.first_tour.select_departure(item)        
        self.arrival_date_time = json2date(item.DatoTid).addMinutes(item.sejltid)
        
    }

    self.get_arrival_date_time = function(item) {
        console.log(item.sejltid)
    }

    self.select_arrival_harbor = function (item) {

        self.first_tour.loading_departures = true
        self.first_tour.selected_departures = null
       
        

        if (self.selected_ticket_type.type == "retur") {
            self.first_tour.select_arrival_harbor(item)
            var retur_tour = self.get_last_tour();
            retur_tour.select_departure_harbor(item);
            retur_tour.select_arrival_harbor(self.first_tour.selected_departure_harbor).then(function () { self.departures_is_selected = true })
        } else {
            self.first_tour.select_arrival_harbor(item).then(function () {

                
                self.departures_is_selected = true
            })
        }
    } 

    self.select_ticket_type = function () {        
        mainService.selected_ticket_type = self.selected_ticket_type

        if (self.selected_ticket_type.type == "retur") {                     
            var tour = tourService.add_tour()
        } else if (self.selected_ticket_type.type == "nonstop" || self.selected_ticket_type.type == "evening") {
            //silkeborg_nonstop_overfart_id
            self.first_tour.select_arrival_harbor(self.first_tour.selected_departure_harbor)
        }        
    }
    

    self.start_date_changed = function () {
        tourService.tours.splice(1)
        var first = tourService.get_first()
        first.departure_date = $("#datepicker").datepicker('getDate').toISOString()
        tourService.start_date = $("#datepicker").datepicker('getDate').toISOString()
                
        

        mainService.get_harbors_by_date(first.departure_date).then(function (result) {
            first.departure_harbors = result
            first.prioritized_departure_harbors = first.get_prioritized_harbors(result)            
        })

        self.selected_index = 1
    }

    self.md_datepickerStartDate = null
    self.md_start_date_changed = function() {

        self.datepickerStartDate = $filter('date')(self.md_datepickerStartDate, "dd-MM-yyyy")

        tourService.tours.splice(1)
        var first = tourService.get_first()
        first.departure_date = self.md_datepickerStartDate.toISOString()
        tourService.start_date = self.md_datepickerStartDate.toISOString()

        mainService.get_harbors_by_date(first.departure_date).then(function (result) {
            first.departure_harbors = result
            first.prioritized_departure_harbors = first.get_prioritized_harbors(result)
        })

        self.selected_index = 1
        
    }
   
    self.reset_datepicker = function () {
        
        self.is_persons_selected = false
        mainService.is_persons_selected = false
        self.close_selected_ticket_type()
        self.reset_departure()
        self.selected_index = 0 
    }

    self.reset_departure = function () {
        
        self.first_tour.selected_departure_harbor = null

        self.selected_index = 1
        
        self.selected_ticket_type = null
        self.is_persons_selected = false

        tourService.tours.splice(1)
        var first = tourService.get_first()
        first.departure_date = $("#datepicker").datepicker('getDate').toISOString()
        tourService.start_date = $("#datepicker").datepicker('getDate').toISOString()
        mainService.get_harbors_by_date(first.departure_date).then(function (result) {
            first.departure_harbors = result
            first.prioritized_departure_harbors = first.get_prioritized_harbors(result)
        })
    }


    self.persons_selected = function (val) {

        tourService.selected_unit_type = self.selected_unit_type
        self.is_persons_selected = val
        mainService.is_persons_selected = val        
        $rootScope.$broadcast("person_is_selected");
        if (val) {
            self.selected_index = -1
           
        } 
    }

    self.selected_tour = null
    self.settings = mainService.settings    
    self.number_of_persons = mainService.number_of_persons;

    self.is_in_focus = function (event, item) {
        item.Amount = null
    }

    self.count_number_of_persons = function () {
        var sum = 0
        $.each(self.selected_unit_type.person_types, function (index, elm) {
            var value = elm.Amount != null ? elm.Amount : 0
            sum += value;
        })
        self.number_of_persons = sum        
        mainService.number_of_persons = sum
        
    
    }

    $scope.doBlur = function ($event) {
        var target = $event.target;

        // do more here, like blur or other things
        target.blur();
    }


    self.input_leave = function (event, item) {        
        if (item.Amount == null) {
           // item.Amount = 0;
        }
        self.count_number_of_persons()
    }

    self.addToItem = function (item, value) {
        var item_value = parseInt(item.Amount)

        if (item_value) {
            item.Amount = item_value + value
        } else {
            item.Amount = 1
        }
        self.count_number_of_persons()
    }

    self.addToNumberOfItem = function (item, value) {
        
        var item_value = parseInt(item.number_of_items)

        if (item_value) {
            item.number_of_items = item_value + value
        } else {
            item.number_of_items = 1
        }
        
    }

    
    self.start_date = tourService.start_date
    self.selected_unit_type = tourService.selected_unit_type

    self.unit_type_selected = function () {
        tourService.selected_unit_type = self.selected_unit_type
    }

    self.loading = false

    self.map_harbors = mainService.map_harbors



    
    $scope.$on("done", function () {



        var possible_dates = mainService.possible_dates;


        self.notPosibleDeparturesPredicate = function (date) {
            return $filter('filterDepartureDates')(date, possible_dates);
        };

        self.unit_types = mainService.unit_types
        self.departure_harbors = mainService.harbors

        self.tours = tourService.tours
        // INitializing first tour
        self.tours[0].departure_harbors = self.departure_harbors

        // Select first unittype

        if (self.settings.cykel_enhedsttype_id) {

            var s = $.grep(self.unit_types, function (elm) {
                return self.settings.cykel_enhedsttype_id == elm.Enhedstype_id
            })

            if (s.length > 0) {
                self.selected_unit_type = s[0]
                self.unit_type_selected()
            } else {
                alert("manglende opsætning af cykel_enhedsttype_id i administration-settings !")
            }
        }

        self.tours = tourService.tours

        self.first_tour = self.tours[0]
        self.prioritized_harbors = mainService.prioritized_harbors

        $timeout(function () {
            
            var maxDate = json2date(mainService.maxDate)
            

            self.maxDate = new Date(
               maxDate.getFullYear(),
               maxDate.getMonth(),
               maxDate.getDate());




           

            
            function available_dates(date) {
                //dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();

                var pd = $.grep(possible_dates, function (elm) {
                    
                    return moment(date).format("DD-MM-YYYY") == moment(elm).format("DD-MM-YYYY")
                })

                if (pd.length > 0) {
                    return [true, ""];                    
                } else {
                    return [false, "", "Unavailable"];                    
                }
            }

            $("#datepicker").datepicker("option", "maxDate", self.maxDate)
            $('#datepicker').datepicker("option", "beforeShowDay", available_dates);            

        }, 300)
        
    });

    //self.price_total = function () {
    //    return tourService.price_total()
    //}
    
    self.enable_add_tour = function () {
        return tourService.enable_add_tour()
    }

   
   
    self.delete_tour = function () {
        tourService.delete_tour()
    }
      
    self.toggleSide = function (componentId) {        
        $mdSidenav(componentId).toggle();        
    }

    self.init = function () {

    }
    
}
