﻿angular.module('myApp.services')
  .factory('mainService', ['$http', '$q','$location','$translate', mainService]);

function mainService($http, $q, $location, $translate) {

    //    var api_path = "http://dev.compuferry.dk/api/diverse"
    var api_path = ""
    var o = {
        home_harbor: null,
        harbors: [],
        list: [],
        ruter: [],
        departure_date: null,
        selected_departure_harbor: null,
        selected_arrival_harbor: null,
        departures: [],
        tours: [],
        routes: [],
        company_id: 8,
        unit_types: [],
        language_codes: [],
        str_routes: "",
        person_types: [],
        map_harbors: [],
        portalId: 2, // SET portalId here ** ABC
        booking_state: -2,
        start_date: new Date(),
        number_of_persons: 0,
        map_loaded: false,
        str_router: "",
        settings: {},
        price_total: 0,
        is_persons_selected: false,
        prioritized_harbors: null,
        ticket_types: [
            { type: 'single', text: 'Enkeltbillet', Languages: { DA: 'Enkeltbillet', EN: 'One way' } },
            { type: 'retur', text: 'Returbillet', Languages: { DA: 'Returbillet', EN: 'Roundtrip' } },
            { type: 'nonstop', text: 'Non-stop rundtur fra Silkeborg', Languages: { DA: 'Non-stop rundtur fra Silkeborg', EN: 'Non-stop tour from Silkeborg' } },
            { type: 'evening', text: 'Aftensejlads', Languages: { DA: 'Aftensejlads', EN: 'Evening sail' } },
            
            //{ type: 'advanced', text: 'Avanceret' }
            
        ],
        selected_ticket_type: null,
        is_mobile: false,
        maxDate: null,
        possible_dates: []
    };

    insuloj = [
        {
            id: 3,
            name: 'Skarø',
            url: 'http://www.ø-hop.dk/skaroe/'
        }
    ]

    o.book = function (unit_type, tours, user_info) {
               
        var unit_type_id = unit_type.Enhedstype_id
        var number_of_items = 0 

        if (unit_type.KravStk) {
            number_of_items =  unit_type.number_of_items
        }

        

        if (number_of_items > 0) {

            var bikes = $.grep(o.unit_types, function (elm) {
                return elm.Enhedstype_id == o.settings.cykel_enhedsttype_id
            })

            

            if (bikes.length > 0) {                
                unit_type_id = bikes[0].Enhedstype_id

            }
        } else {

            var bikes = $.grep(o.unit_types, function (elm) {
                return elm.Enhedstype_id != o.settings.cykel_enhedsttype_id
            })

            if (bikes.length > 0) {                
                unit_type_id = bikes[0].Enhedstype_id

            }
        }

        var person_types = []
        $.each(unit_type.person_types, function (index, elm) {
            if (elm.Amount > 0) {
                person_types.push({Id: elm.id , Amount: elm.Amount })
            }
        })

        var tour_ids = []

        $.each(tours, function (index, elm) {
            var departure_ids = []
            departure_ids.push(elm.selected_departure.Afgangs_id)

            $.each(elm.selected_departure.path, function(index2, elm2) {
                departure_ids.push(elm2.Afgangs_id)
            })

            tour_ids.push(departure_ids)
        })

        return $q(function (resolve, reject) {

            
            var booking = {
                "TicketType": o.selected_ticket_type.type,
                    "DepartureIds": tour_ids,
                    "PersonTypes": person_types,
                    "UnitType": {
                        "Id": unit_type_id,
                        "NumberOfItems": number_of_items
                    },
                    "ReturnUrl": $location.absUrl()
            }

            

            if (user_info != undefined) {

                // IF user_info then it is administrativ booking
                booking.user_info = user_info                

                $http({
                    method: 'POST',
                    url: '/api/Booking/BookIslandJump',
                    data: booking
                })
                .success(function (result) {
                    return resolve(result);
                }).error(function (result) {
                    return reject(result)
                });

            } else {
                $http({
                    method: 'POST',
                    url: '/api/Booking/BookIslandJump',
                    data: booking
                })
                .success(function (result) {
                    return resolve(result);
                }).error(function (result) {
                    return reject(result)
                });
            }            
        });
    }

    o.get_all = function () {
        return $q(function (resolve, reject) {
            
            var str_today = moment(new Date()).format("DD-MM-YYYY")
            
            //$http.get(api_path + 'api/UnitTypes/Get?portalId=' + o.portalId + '&start_date=' + str_today + '&start_datetime=' + new Date().toJSON())
            $http.get(api_path + 'api/UnitTypes/Get?portalId=' + o.portalId + '&start_date=' + str_today + '&start_datetime=' + moment(new Date()).format('YYYY-MM-DDTHH:mm:ss'))
                
            .success(function (result) {                
                o.language_codes = result.language_codes;

                $.each(result.person_types, function(index, elm) {
                    if (elm.Amount == 0) {
                        elm.Amount = null
                    }
                })

                o.person_types = result.person_types;

                // Getting persontypes 
                $.each(result.unit_types, function (index, elm) {
                    elm.person_types = $.grep(o.person_types, function (elm2) {
                                
                        return   elm.person_type_ids.split(",").map(Number).indexOf(elm2.id) > -1                        
                    })
                });
                o.start_date = new Date()
                o.unit_types = result.unit_types;
                o.str_routes = result.str_routes;
                o.routes = result.routes;
                o.maxDate = result.maxDate[0].value                
                var array_ruter = []
                
                $.each(result.routes, function (index, elm) {
                    array_ruter.push(elm.Rute_id)
                })
                o.ruter = array_ruter

                o.harbors = result.harbors
                o.prepare_harbors(result.harbors)
                o.possible_dates = result.possible_dates

                return resolve(result);
            });
        });
    }

    o.get_harbors_by_date = function (start_date) {
        return $q(function (resolve, reject) {
            
            
            var str_start_date
            if(typeof(start_date) == "object") {
                str_start_date = moment(start_date).format("DD-MM-YYYY")
            } else {
                str_start_date = start_date
            }

            $http.get(api_path + 'api/UnitTypes/GetHarborsByDate?portalId=' + o.portalId + '&start_date=' + str_start_date)
            .success(function (result) {
                
                return resolve(o.prepare_harbors(result));
            });
        });
    }

    o.get_prioritized_harbors = function (harbors) {
        var prioritized_harbors = []
        alert("mainservice linie 228")
        
        //$.each(o.settings.primary_harbors.trim().split(/\s*,\s*/), function (index, elm) {
        
        //    var h = $.grep(harbors, function (elm2) {               
        //        return elm2.initialer.toLowerCase().trim() == elm.trim().toLowerCase()
        //    })            
        //    if (h.length > 0) {
        //        prioritized_harbors.push(h[0])
        //    }

        //})
        
        return prioritized_harbors
    }

    o.prepare_harbors = function (harbors) {

        var new_harbors = []
        $.each(harbors, function (index, elm) {

            var arrival_harbors = elm.arrival_harbor_ids
            if (elm.secondary_arrival_harbor_ids) {
                arrival_harbors = arrival_harbors + ", " + elm.secondary_arrival_harbor_ids
            }

            var is_priorytized =  o.settings.primary_harbors.trim().split(/\s*,\s*/).indexOf(elm.initialer.trim().toLowerCase()) > -1

            var h = {
                id: elm.havne_id,
                lat: parseFloat(elm.Latitude),
                lng: parseFloat(elm.Longitude),
                name: elm.Navn,                
                arrival_harbor_ids: arrival_harbors,
                secondary_arrival_harbor_ids: elm.secondary_arrival_harbor_ids,
                css_class: is_priorytized ?  'marker_green' : 'marker_dot',
                label_status: -2,
                tour_number: [],
                state: -2, /* initial state */
                is_home: false,
                initialer: elm.initialer,
                is_priorytized: is_priorytized
            }

            $.each(insuloj, function (i, e) {
                if (e.id == elm.havne_id) {
                    h.url = e.url
                    return
                }
            })

            //o.map_harbors.push(h)
            new_harbors.push(h)
        })
        o.map_harbors = new_harbors;
        o.harbors = o.map_harbors
      //  o.prioritized_harbors = o.get_prioritized_harbors(o.harbors)
        return o.harbors

    }



    o.route_paths = function (harbors) {

        var paths = []
        $.each(harbors.split(","), function (index, elm) {
            var h = o.get_harbor_by_id(elm)
             paths.push( {
                "latitude": parseFloat(h.Latitude),
                "longitude": parseFloat(h.Longitude)
            })
        })
        return paths
    }

    o.get_harbor_by_id = function (id) {
        return $.grep(o.harbors, function (elm) {
            return elm.id == parseInt(id)
        })[0]
    }

    o.get_arrival_harbors = function (departure_harbor) {
        // split arrival harbors "1,2,3" to array of numbers
        var r = departure_harbor.arrival_harbor_ids.split(',').map(Number);
        
        return $.grep(o.harbors, function (elm) {        
            return r.indexOf(elm.id) > -1 && departure_harbor.id != elm.id
        })
    }   
  

   

    return o;
};