﻿angular.module('myApp.services')
  .factory('mapService', ['$http', '$q', MapService]);

function MapService($http, $q) {

    //    var api_path = "http://dev.compuferry.dk/api/diverse"
    var api_path = ""
    var o = {
    };

    o.get_oferry_position = function () {
        return $q(function (resolve, reject) {
            $http.get(api_path + '/api/diverse/GetPosition')
            .success(function (result) {                
                return resolve(result);
            });
        });
    }

   

    return o;
};