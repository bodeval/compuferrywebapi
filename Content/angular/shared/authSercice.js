﻿'use strict';
angular.module('myApp.services').factory('authService', ['$http', '$q', 'localStorageService','$rootScope', '$translate', function ($http, $q, localStorageService,$rootScope, $translate) {

    var self = this;
    var serviceBase = '/';
    var o = {};

    o.authentication = {
        isAuth: false,
        userName: "",
        language: "",
        roles: "",
        site_id: "",
        isAdmin: false
    };

    o.broadcast = function () { $rootScope.$broadcast("auth") }
    o.listen = function (callback) { $rootScope.$on("auth", callback) }


    o.saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'cRegister', registration).then(function (response) {
            return response;
        });

    };

    o.login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            
            localStorageService.set('authorizationData', {
                token: response.access_token,
                userName: response.userName,
                language: response.language,
                roles: response.roles,
                site_id: response.site_id,
            
            });

            o.authentication.isAuth = true;
            o.authentication.userName = response.userName;
            o.authentication.roles = response.roles;
            o.authentication.isAdmin = o.authentication.roles.indexOf('1') > -1 

            o.authentication.language = response.language;
          

            

            //$translate.use(response.language);

            o.broadcast()
            deferred.resolve(response);

        }).error(function (err, status) {
            o.logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    o.logOut = function () {

        localStorageService.remove('authorizationData');

        o.authentication.isAuth = false;
        o.authentication.isAdmin = false;
        o.authentication.userName = "";
        o.broadcast()
      
    };

    o.fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            o.authentication.isAuth = true;
            o.authentication.userName = authData.userName;
            o.authentication.roles = authData.roles;
            o.authentication.isAdmin = o.authentication.roles.indexOf('1') > -1
            o.authentication.language = authData.language;
            o.authentication.site_id = authData.site_id;           
            
        }
    }

    o.register = function (username, password) {
            return $q(function (resolve, reject) {               

                $http({
                    method: 'POST',
                    url: serviceBase + 'api/Account/Register',
                    
                    data: { Email: username, Password: password, ConfirmPassword: password }

                }).success(function (result) {
                    return resolve(result);
                });
            });
     }

    return o;
}]);