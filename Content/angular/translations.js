﻿angular.module('myApp').config(['$translateProvider', function ($translateProvider) {
    $translateProvider.translations('en', {
        edit: 'Edit',
        cancel: 'Cancel',       
        save: "Save",
        "continue": "Continue",
        select: "Select",
        number_of_persons_total: "Number of persons total",
        "select_persons": "Select persons",
        tip_select_journey: "Choose where you want to start the journey",
        tip_use_map_right: "TIP: Try using the card to the right",
        add_tour: "Add crossing",
        powered_by: "Powered by",
        how_do_u_travel: "How do you travel?",
        Number: "Number",
        from: "from",
        to: "to",
        Select_harbour: "Select harbour",
        Enter_date: "Enter date",
        Delete_last_crossing: "Delete last crossing",
        hh_mm: "hh:mm",
        Sold_out: "Sold out",
        To_many_people: "To many persons!",
        No_vacant_departures: "No vacant departures on selected date.",
        Available_departure: "Available departures on selected date.",
        Locked: "Locked",
        Start_travel_here: "Start travel here",
        Propose_travel_here: "Continue travel here",
        Read_about_destination: "Read about the destination",
        To_many_bikes: "To many bikes",
        of: "of",
        Departure_date: "Departure",
        Start_from: "Start from",
        single: "Enkeltbillet",
        retur: "Returbillet",
        nonstop: "Non-stop rundtur fra",
        evening: "Aftensejlads fra",
        advanced: "Avanceret planlægning (dagsbillet)",
        Select_date: 'Select date',
        Sail_from: 'Sail from',
        Persons: 'Persons',
        enter_travel_information: 'Enter travel information',
        Choose_departure_from: 'Choose departure from',
        Choose_departure: 'Choose departure',
        Choose_departures: 'Choose departures',
        Alphabetical: 'Alphabetical',
        Select_ticket_type: 'Select ticket type',
        Select_destination: 'Select destination',
        Select_another_destination: 'Select another destination',
        Select_another_ticket_type: 'Select another ticket type',
        Ticket_type: 'Ticket type',
        Sold_Out: 'Sold Out'
        

        
    });
    
    $translateProvider.translations('da', {
        edit: 'Ret mig',
        cancel: 'Annuller',
        save: "Gem",
        "continue": "Fortsæt",
        select: "Vælg",
        number_of_persons_total: "Antal personer ialt",
        "select_persons": "Vælg personer",
        tip_select_journey: "Vælg hvor du vil starte rejsen",
        tip_use_map_right: "Klik på de øer du ønster at besøge",
        add_tour: "Tilføj overfart",
        powered_by: "Drevet af",
        how_do_u_travel: "Hvordan rejser du?",
        Number: "Antal",
        from: "fra",
        to: "til",
        Select_harbour: "Vælg havn",
        Enter_date: "Angiv dato",
        Delete_last_crossing: "Slet sidste overfart",
        hh_mm: "kl.",
        Sold_out: "Udsolgt",
        To_many_people: "Too many people!",
        No_vacant_departures: "Ingen ledige afgange på valgte dato.",
        Available_departure: "Ledige afgange på valgte dato",
        Locked: "Låst",
        Start_travel_here: "Start rejse her",
        Propose_travel_here: "Forsæt rejse her",
        Read_about_destination: "Læs om destinationen",
        To_many_bikes: "Der er ikke nok ledige pladser til cykler!",
        of: "af",
        Departure_date: "Afrejse dato",
        Start_from: "Start fra",
        single: "Enkeltbillet",
        retur: "Returbillet",
        nonstop: "Non-stop rundtur fra",
        evening: "Aftensejlads fra",
        advanced: "Avanceret planlægning (dagsbillet)",
        Select_date: 'Vælg dato',
        Sail_from: 'Sejl fra',
        Persons: 'Personer',
        enter_travel_information: 'Angiv rejseoplysninger', 
        Choose_departure_from: 'Vælg afgang fra',
        Choose_departure: 'Vælg afgang',
        Choose_departures: 'Vælg afgange',
        Alphabetical: 'Alfabetisk',
        Select_ticket_type: 'Vælg billettype',
        Select_destination: 'Vælg destination',
        Select_another_destination: 'Vælg en anden destination',
        Select_another_ticket_type: 'Vælg en anden billettype',
        Ticket_type: 'Billettype',
        Sold_Out: 'Udsolgt'

    });
    
   
  


    $translateProvider.preferredLanguage('en');
}]);

