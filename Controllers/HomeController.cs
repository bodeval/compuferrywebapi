﻿

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OeHopWebPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //System.Web.Http.IHttpActionResult result = new UnitTypesController().Get(1);

            //var unserializedContent = JsonConvert.DeserializeObject(result.ToString());

            //var t = Json(unserializedContent);

           

            
            ViewBag.page_title = "CompuferryApi v1";



            return View();
        }

        internal dynamic GetPortalSettingsFromDB(int portal_id)
        {
            JObject result = new JObject();
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"select p.*, 

                        (
                    SELECT max(A.Datotid) as value
                    FROM Afgange A
                    inner join overfarter O on O.Overfart_id = A.overfart_id
                    where O.Rute_Id in (select RouteId from PortalRoute where portalid = {0}  ) and a.afgangs_id > 0) as maxDate

                        from portalSettings p
                        where portal_id = {0} order by id desc", portal_id);
                SqlCommand command = new SqlCommand(sql, conn);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    result = JObject.Parse((string)reader["settings"]);
                    result["maxDate"] = (DateTime)reader["maxDate"];
                }

                reader.Close();

                return result;
            }
        }

    }
}
