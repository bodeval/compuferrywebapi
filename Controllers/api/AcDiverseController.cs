﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CompuFerryManager;
using HtmlAgilityPack;
using System.Net.Http.Headers;





namespace OeHopWebPortal.Controllers.api
{
    public class AcDiverseController : BaseApiController
    {

      


        [HttpGet]
        public IHttpActionResult GetRoutes(string ruter)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string rute_sql = String.Format(@"SELECT *, 
                     stuff((select distinct ',' + CAST(StartHavn_Id as varchar(10))
                     from overfarter  where Rute_Id = R.Rute_id
                     for xml path('')),1,1,'') harbor_ids
                    FROM Rute R where Rute_id in ({0})", ruter);
                List<Dictionary<string, object>> routes = GetJsonList(rute_sql, conn);

                foreach (dynamic item in routes)
                {
                    string overfart_sql = String.Format(@"
                                      select o.[Navn]
                                      ,o.[Retur_id]      
                                      ,o.[selskabs_id]      
                                      ,o.[StartHavn_Id]
                                      ,o.[SlutHavn_Id]
                                      ,o.[Rute_Id]
                                      ,o.[Bookingkundetype]
                                      ,o.[emergency]
	                                  , DH.Navn as departure_harbor_name
	                                  , DH.Latitude as departure_harbor_lat
	                                  , DH.Longitude as departure_harbor_lng
	                                  , AH.Navn as arrival_harbor_name
	                                  , AH.Latitude as arrival_harbor_lat
	                                  , AH.Longitude as arrival_harbor_lng
                                       , direction 
                                        from overfarter o
                                        inner join Havne DH on DH.havne_id = o.StartHavn_Id
                                        inner join Havne AH on AH.havne_id = o.SlutHavn_Id
                                        where o.Rute_Id = ({0})", item["Rute_id"]);
                    item["crossings"] = GetJsonList(overfart_sql, conn);
                }
                return Ok(routes);
            }
        }

        [HttpGet]
        public IHttpActionResult GetHarborsByRute(string ruter)
        {
            var harbourManager = new HarbourManager();
            var harbours = harbourManager.GetHarborsByRute(ruter);

            return Ok(harbours);
        }

        [HttpGet]
        public IHttpActionResult GetPosition()
        {
            try
            {


                //https://www.marinetraffic.com/dk/ais/details/ships/shipid:155487/mmsi:219016906/imo:9628647/vessel:FAABORG_III/

                //https://www.marinetraffic.com/dk/ais/details/ships/shipid:155641/mmsi:219017917/imo:9664524/vessel:STRYNOE/

                //https://www.marinetraffic.com/dk/ais/details/ships/shipid:153535/mmsi:219000742/imo:9169794/vessel:HOJESTENE/



                string Url = " https://www.marinetraffic.com/en/ais/details/ships/shipid:155487/mmsi:219016906/vessel:FAABORG";
                HtmlWeb web = new HtmlWeb();
                HtmlDocument doc = web.Load(Url);


                //string summary = doc.DocumentNode.SelectNodes("//*[@class=\"details_data_link\"]")[0].InnerText;
                string str_coordinates = doc.DocumentNode.SelectNodes("//*[@class=\"details_data_link\"]")[0].InnerText;

                List<string> names = str_coordinates.Split('/').ToList<string>();

                var result = new { latitude = names[0].Split('&').ToList<string>()[0], longitude = names[1].Split('&').ToList<string>()[0] };

                return Ok(result);
            }
            catch (Exception e)
            {
                return NotFound();
            }

        }


        [HttpGet]
        public IHttpActionResult GetDeparturesByDate(DateTime departure_date, int selskab_id = 0, string ruter = "")
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string rute_query = "";
            if (ruter != "")
            {
                rute_query = String.Format("And Rute_id in({0})", ruter);
            }

            string sql = String.Format(@" declare
                        @str_date varchar(30) = '{1}'
                        declare
                        @selskab_id int = {0}

                        SELECT a.Overfartstid, a.Afgangs_id, a.DatoTid, o.Navn, o.Overfart_id, o.Rute_Id, Sh.Navn as starthavn , uh.Navn as sluthavn , a.Fartoj_id
                              , O.StartHavn_Id as starthavn_id
	                          , O.SlutHavn_Id as sluthavn_id
                              , F.Navn as ferry_name
                              , A.BookSelvProcent
                              , EnhedsTypeBookSelvPct
                              , PersonBookSelvPct
                              , tonnage
                              , Akt_lukketid_IfBookings
                              , Udsolgt
                              , Locked
                              , Akt_lukketid  
	                          , 

                                F.MaxDakLang as MaxLength, F.MaxDakHLang as MaxHeight, F.MaxEnh as MaxUnitsAmount, A.MaxPers, 
                                          ISNULL(A.Tonnage, F.MaxVagt) as MaxWeight, 
                                          ISNULL((SELECT sum(Vagt) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumWeight, 
                                          ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumPersons,
                                          ISNULL((SELECT sum(AntEnh) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumUnitsAmount,
                                          ISNULL((SELECT sum(Langde) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumLength, 
                                          ISNULL((SELECT sum(AntalVoksne) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumAdults, 
                                          ISNULL((SELECT sum(AntalBoern) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumChildren, 
                                          ISNULL((SELECT sum(Antaloeboer) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumIslanders ,
                                          ISNULL((SELECT count(*) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumBookings ,

                             isnull( 
	                         (
		                         select top 1 ia.Afgangs_id From afgange IA
			                        inner join overfarter IO on  io.Overfart_id = IA.Overfart_id
			                        where iO.StartHavn_Id = O.SlutHavn_Id and IA.DatoTid > A.DatoTid  
			                        and convert(date,IA.DatoTid)  = convert(date,@str_date)
			                        and IA.Fartoj_id = a.Fartoj_id
			
			                        order by IA.DatoTid
		                          )
                            , 0) as naeste

                          FROM [Afgange] A
	                        inner join Overfarter o on o.Overfart_id = A.Overfart_id
	                        inner join Havne SH on SH.havne_id = O.StartHavn_Id
                            Inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                            inner join Havne UH on UH.havne_id = O.SlutHavn_Id

                               ", selskab_id, departure_date);

            if (selskab_id > 0)
            {
                sql += String.Format(@"where convert(date,@str_date) = convert(date,a.Datotid)
			                    and a.selskabs_id = @selskab_id
			                    and a.Fartoj_id in (
				                    SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) {0})
                    order by a.DatoTid", rute_query);
            }
            else
            {
                sql += String.Format(@"where convert(date,@str_date) = convert(date,a.Datotid)			                                           
			                            and a.Fartoj_id in (
				                        SELECT Fartoj_id  FROM Driftsplan dp  WHERE  (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)){0})
                                        order by a.DatoTid", rute_query);
            }

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int company_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                // sprogkoder
                string unit_type_language_phrases = "";
                string language_sql = String.Format("SELECT C.code, L.language FROM company_language_codes C inner join language_codes L on L.code = C.code where selskabs_id = {0} order by position", company_id);
                List<Dictionary<string, object>> language_codes = GetJsonList(language_sql, conn);

                // Unittypes
                foreach (dynamic item in language_codes)
                {
                    string code = item["code"];
                    unit_type_language_phrases += String.Format(" , (SELECT Navn FROM EnhTypSprogTekster where Selskabs_id = {0} and SprogKode = '{1}' and Enhedstype_id = E.Enhedstype_id) as {1} ", company_id, code);
                }
                string sql_unit_types = String.Format(@"select *, ISNULL(Navn,'') as name 
                                    	  ,stuff((select ',' + CAST([person_type_id] as varchar(10))
                                           from  person_types_unit_types  where  E.Enhedstype_id = unit_type_id
                                           for xml path('')),1,1,'') person_type_ids {1}
                                           from Enhedstyper E where active = 1 and Selskabs_Id = {0}", company_id, unit_type_language_phrases);

                List<Dictionary<string, object>> unit_types = GetJsonList(sql_unit_types, conn);

                // Ruter todo: 
                string str_routes = "1, 3, 8 , 10, 11";

                // PersonTypes
                string person_type_language_phrases = "";
                foreach (dynamic item in language_codes)
                {
                    string code = item["code"];
                    person_type_language_phrases += String.Format(", (SELECT name FROM person_type_translations where person_type_id = p.id and code = '{0}') as {0} ", code);
                }

                string sql_person_types = String.Format("select * {1} from person_types p where company_id = {0}", company_id, person_type_language_phrases);

                List<Dictionary<string, object>> person_types = GetJsonList(sql_person_types, conn);

                // result
                Dictionary<string, object> result = new Dictionary<string, object>();

                // Harbors
                var harbourManager = new HarbourManager();
                var harbours = harbourManager.GetHarborsByRute(str_routes);


                string rute_sql = String.Format(@"SELECT *, 
                     stuff((select distinct ',' + CAST(StartHavn_Id as varchar(10))
                     from overfarter  where Rute_Id = R.Rute_id
                     for xml path('')),1,1,'') harbor_ids
                    FROM Rute R where Rute_id in ({0})", str_routes);
                List<Dictionary<string, object>> routes = GetJsonList(rute_sql, conn);

                foreach (dynamic item in routes)
                {
                    string overfart_sql = String.Format(@"
                                      select o.[Navn]
                                      ,o.[Retur_id]      
                                      ,o.[selskabs_id]      
                                      ,o.[StartHavn_Id]
                                      ,o.[SlutHavn_Id]
                                      ,o.[Rute_Id]
                                      ,o.[Bookingkundetype]
                                      ,o.[emergency]
	                                  , DH.Navn as departure_harbor_name
	                                  , DH.Latitude as departure_harbor_lat
	                                  , DH.Longitude as departure_harbor_lng
	                                  , AH.Navn as arrival_harbor_name
	                                  , AH.Latitude as arrival_harbor_lat
	                                  , AH.Longitude as arrival_harbor_lng
                                        from overfarter o
                                        inner join Havne DH on DH.havne_id = o.StartHavn_Id
                                        inner join Havne AH on AH.havne_id = o.SlutHavn_Id
                                        where o.Rute_Id = ({0})", item["Rute_id"]);
                    item["crossings"] = GetJsonList(overfart_sql, conn);
                }






                result.Add("unit_types", unit_types);
                result.Add("language_codes", language_codes);
                result.Add("routes", routes);
                result.Add("person_types", person_types);
                result.Add("harbors", harbours);

                return Ok(result);

            }
        }

        [HttpGet]
        public HttpResponseMessage Svg(string label = "test")
        {


          string svg = @"<svg width=""90"" height=""40"" xmlns=""http://www.w3.org/2000/svg"" xmlns:svg=""http://www.w3.org/2000/svg"">" +
                "<defs>" +
                @"<filter id=""svg_3_blur"">" +
                @"<feGaussianBlur stdDeviation=""0"" in=""SourceGraphic"" />" +
                @"</filter>" +
                @"</defs>" +
                @"<g>" +
                @"<rect opacity=""1"" filter=""url(#svg_3_blur)"" id=""svg_3"" height=""27"" width=""80"" y=""2"" x=""2"" stroke-linecap=""null"" stroke-linejoin=""null"" stroke-dasharray=""null"" stroke=""#009688"" fill=""#009688"" />" +
                @"<text fill=""#ffffff"" stroke=""#5fbf00"" stroke-width=""0"" stroke-dasharray=""null"" stroke-linejoin=""null"" stroke-linecap=""null"" x=""40"" y=""19.63281"" id=""svg_1"" font-size=""12"" font-family=""Sans-serif"" text-anchor=""middle"" xml:space=""preserve"" font-weight=""bold"">" + label + "</text>" +
                @"<polygon opacity=""1"" stroke=""#009688"" transform=""rotate(-31.237993240356445 52.6697807312012,31.500000000000007) "" points=""56.36662673950195,31.5 48.97293472290039,35.76874923706055 48.97293472290039,27.231250762939453 56.36662673950195,31.5 "" strokeWidth=""1"" strokecolor=""#5fbf00"" fill=""#009688"" edge=""8.5375"" orient=""x"" sides=""3"" shape=""regularPoly"" id=""svg_7"" cy=""30"" cx=""26"" />" +
                "</g>" +
                "</svg>";
            
          
            

            
           HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            
            httpResponseMessage.Content = new StringContent(svg);
            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("image/svg+xml");
            httpResponseMessage.StatusCode = HttpStatusCode.OK;

            return httpResponseMessage;



        }
    }
}
