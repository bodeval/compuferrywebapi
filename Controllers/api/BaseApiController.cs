﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Runtime.Caching;
using System.Web.Http.Cors;

namespace OeHopWebPortal.Controllers.api
{



    [System.Web.Mvc.SessionState(SessionStateBehavior.ReadOnly)]
    //[Authorize(Roles = "Administrator")]

    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    //[EnableCors(origins: "*", headers: "*", methods: "*")]

    public class BaseApiController : ApiController
    {


        internal dynamic GetSettings(int portal_id)
        {
            string cachname = "portal_settings_" + portal_id;
            dynamic portal_settings = MemoryCache.Default[cachname] as dynamic;
            //if (portal_settings == null)
            //{
                portal_settings = GetPortalSettingsFromDB(portal_id);

                MemoryCache.Default[cachname] = portal_settings;
            //}



            return portal_settings;
        }

        internal dynamic GetPortalSettingsFromDB(int portal_id)
        {
            JObject result = new JObject();
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"select * from portalSettings where portal_id = {0} order by id desc", portal_id);
                SqlCommand command = new SqlCommand(sql, conn);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    result = JObject.Parse((string)reader["settings"]);
                }
                reader.Close();

                return result;
            }
        }

        internal dynamic GetFavoritTours(int portal_id)
        {
            dynamic portal_favorit_tours = MemoryCache.Default["portal_favorit_tours"] as dynamic;
            if (portal_favorit_tours == null || 1 == 1)
            {
                portal_favorit_tours = GetPortalFavoritTours(portal_id);
                MemoryCache.Default["portal_favorit_tours"] = portal_favorit_tours;
            }
            return portal_favorit_tours;
        }

        internal dynamic GetPortalFavoritTours(int portal_id)
        {
            JObject result = new JObject();
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"select * from portal_favorit_tours where portal_id = {0} order by position", portal_id);


                List<Dictionary<string, object>> favorit_tours = GetJsonList(sql, conn);


                return favorit_tours;
            }
        }

        internal string GetConnectionString(string connection_string)
        {
            string sConn = ConfigurationManager.ConnectionStrings[connection_string].ConnectionString;
            return sConn;
        }

        internal static int GetPortalId()
        {

            return Int32.Parse(ConfigurationManager.AppSettings["portal_id"]);

        }


        internal List<Dictionary<string, object>> GetJsonList(string sql, SqlConnection conn)
        {

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            using (SqlCommand cmd2 = new SqlCommand(sql, conn))
            {
                try
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd2);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                } catch(Exception e)
                {

                }
                
            }

            return rows;
        }

    }
}
