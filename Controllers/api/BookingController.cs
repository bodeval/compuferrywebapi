﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CompuFerryManager;
using CompuFerryManager.Models;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Context;

namespace OeHopWebPortal.Controllers.api
{
    public class BookingController : BaseApiController
    {
        private readonly BookingManager _groupedBookingManager;
        private readonly PersonTypeManager _personTypeManager;
        private DepartureManager _departureManager;
        private ILogger _log;

        public BookingController()
        {
            _groupedBookingManager = new BookingManager();
            _personTypeManager = new PersonTypeManager();
            _departureManager = new DepartureManager();
            _log = Log.ForContext<CompanyManager>();
        }

        public async Task<IHttpActionResult> Get(int departureId)
        {
            var result = await _groupedBookingManager.Get(departureId);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Book(BookingRequestModel bookingRequest)
        {
            _log.Debug("received request for new booking {0}", bookingRequest);
            if (string.IsNullOrEmpty(bookingRequest.ReturnUrl))
            {
                IEnumerable<string> originValues;
                Request.Headers.TryGetValues("Origin", out originValues);
                bookingRequest.ReturnUrl = originValues.First() ?? "";
            }

            var result = await _groupedBookingManager.Book(bookingRequest, true);
            if (result.Success)
                return Ok(result);
            else
                return Content(System.Net.HttpStatusCode.InternalServerError, string.Join(",", result.MessageList));
        }

        [HttpPost]
        public IHttpActionResult GetBookingsByEmailAndLogin(GetBookingsByEmailAndOrderid request)
        {
            using (LogContext.PushProperty("Correlation", Guid.NewGuid()))
            {
                _log.Debug("GetBookingsByEmailAndLogin for request {@0}", request);
                try
                {
                    var response = new List<Booking>();
                    var result = _groupedBookingManager.GetBookingsByLoginOrderIdAndEmail(request.OrderId, request.Email);
                    var bookingTypes = new List<BookingPersonType>();
                    foreach (var booking in result)
                    {
                        bookingTypes = _groupedBookingManager.GetBookingCompanyPersons(booking.Booking_id);
                        response.Add(booking);
                    }

                    var firstDate = new DateTime();
                    var departures = new List<Departure>();
                    foreach (var booking in result)
                    {
                        departures.Add(_departureManager.GetDeparture(booking.Afgangs_id.Value));
                    }

                    var firstDateNullable = departures.Min(x => x.DatoTid);
                    if (firstDateNullable.HasValue)
                        firstDate = firstDateNullable.Value;

                    var resp = new {bookings = response, personTypes = bookingTypes, startDate = firstDate};
                    _log.Debug("GetBookingsByEmailAndLogin {@0}", resp);
                    return Ok(resp);
                }
                catch (Exception e)
                {
                    return Content(System.Net.HttpStatusCode.InternalServerError, e);

                }
            }
        }

        [HttpPost]
        public IHttpActionResult BookIslandJump(IslandJumpRequestmodel bookingRequest)
        {
            if (string.IsNullOrEmpty(bookingRequest.ReturnUrl))
            {
                IEnumerable<string> originValues;
                Request.Headers.TryGetValues("Origin", out originValues);
                bookingRequest.ReturnUrl = originValues.First() ?? "";
            }

            var result = _groupedBookingManager.BookIslandJump(bookingRequest);
            if (result.Success)
                return Ok(result);
            else
                return Content(System.Net.HttpStatusCode.InternalServerError, string.Join(",", result.MessageList));
        }

        [HttpPost]
        public IHttpActionResult TopUpBooking(TopUpIslandJumpRequestModel bookingRequest)
        {
            if (string.IsNullOrEmpty(bookingRequest.ReturnUrl))
            {
                IEnumerable<string> originValues;
                Request.Headers.TryGetValues("Origin", out originValues);
                bookingRequest.ReturnUrl = originValues.First() ?? "";
            }

            var result = _groupedBookingManager.TopUpIslandJump(bookingRequest);
            if (result.Success)
                return Ok(result);
            else
                return Content(System.Net.HttpStatusCode.InternalServerError, string.Join(",", result.MessageList));
        }
    }
}