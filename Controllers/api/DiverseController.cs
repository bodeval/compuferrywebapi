﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CompuFerryManager;
using HtmlAgilityPack;
using OeHopWebPortal.Models;

namespace OeHopWebPortal.Controllers.api
{
    public class DiverseController : BaseApiController
    {


        [HttpGet]
        public IHttpActionResult ScriptManagerErrors(string site = "")
        {
            string sConn = ConfigurationManager.ConnectionStrings["OnlinebookingNy"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {

                
                string sql = "";
                conn.Open();
                if(site != "")
                {
                    sql = String.Format("select top 1000 * from LogScriptManagerErrors where site = '{0}' order by id desc", site);
                    return Ok(GetJsonList(sql, conn));
                } else
                {
                    sql = String.Format("select top 1000 * from LogScriptManagerErrors order by id desc");


                    string sql_count_site = String.Format(@"select top 50 count(*), site from LogScriptManagerErrors
                                                      group by site
                                                      order by count(*) desc");

                    string sql_count_href = String.Format(@"select top 50 count(*), href from[LogScriptManagerErrors]
                                                      group by href
                                                      order by count(*) desc");



                    string sql_count_errors = "select count(*) from LogScriptManagerErrors";
                    string sql_count_last_hour = "select count(*) from LogScriptManagerErrors WHERE created_at > DATEADD(hh, -1, GETDATE())";
                    string sql_count_last_24_hour = "select count(*) from LogScriptManagerErrors WHERE created_at > DATEADD(hh, -24, GETDATE())";
                    string sql_count_one_day_ago = "select count(*) from LogScriptManagerErrors WHERE created_at > DATEADD(hh, -23, GETDATE()) and created_at<DATEADD(hh, -22, GETDATE())";

                    string sql_count_two_day_ago = "select count(*) from LogScriptManagerErrors WHERE created_at > DATEADD(hh, -47, GETDATE()) and created_at<DATEADD(hh, -46, GETDATE())";



                      Dictionary<string, object> result = new Dictionary<string, object>();

                    List<Dictionary<string, object>> errors = GetJsonList(sql, conn);

                    List<Dictionary<string, object>> count_site = GetJsonList(sql_count_site, conn);

                    List<Dictionary<string, object>> count_href = GetJsonList(sql_count_href, conn);



                    List<Dictionary<string, object>> count_errors = GetJsonList(sql_count_errors, conn);
                    List<Dictionary<string, object>> count_last_hour = GetJsonList(sql_count_last_hour, conn);
                    List<Dictionary<string, object>> count_one_day_ago = GetJsonList(sql_count_one_day_ago, conn);
                    List<Dictionary<string, object>> count_two_day_ago = GetJsonList(sql_count_two_day_ago, conn);

                    List<Dictionary<string, object>> count_last_24_hour = GetJsonList(sql_count_last_24_hour, conn);

                    


                    result.Add("errors", errors);
                    result.Add("count_site", count_site);
                    result.Add("count_href", count_href);

                    result.Add("count_errors", count_errors);
                    result.Add("count_last_hour", count_last_hour);
                    result.Add("count_one_day_ago", count_one_day_ago);
                    result.Add("count_two_day_ago", count_one_day_ago);
                    result.Add("count_last_24_hour", count_last_24_hour);


                    return Ok(result);


                }
                

                
            }
        }


        [HttpGet]
        public IHttpActionResult HandleScriptManagerErrors(string referer, string href, string site, int count, string browser, string server)
        {
            string sConn = ConfigurationManager.ConnectionStrings["OnlinebookingNy"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format("insert into LogScriptManagerErrors(referer, href, site, count, browser, server) values('{0}', '{1}', '{2}', {3}, '{4}', '{5}')", referer, href, site, count, browser, server);
                SqlCommand command = new SqlCommand(sql, conn);

                command.ExecuteNonQuery();
                return Ok();
            }
        }


        [HttpGet]
        public IHttpActionResult GetCustomerBookings(int customer_id, int company_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"SELECT r.rute_id, b.*, a.DatoTid , F.Navn as ferry_name, o.Navn as overfarts_navn, a.Overfartstid as duration, e.Navn as enhedstype_navn
                                              , h.havne_id as start_havn_id
	                                            , he.havne_id as slut_havn_id
	                                            , h.Navn as start_havn
	                                            , he.Navn as slut_havn
                                              FROM Bookinger b
                                              inner join Afgange a on a.Afgangs_id = b.Afgangs_id
                                              inner join Fartojer F on a.Fartoj_id =F.Fartoj_id
                                              inner join Overfarter o on o.Overfart_id = a.Overfart_id
                                               inner join rute r on r.Rute_Id = o.Rute_id
                                               inner join Havne H on h.havne_id =o.StartHavn_Id
											  inner join Havne HE on HE.havne_id =o.SlutHavn_Id
                                              inner join Enhedstyper E on E.Enhedstype_id = b.EnhedsType_id
                                              where b.custid = {0} and b.Selskabs_id = {1}
                                                and CONVERT(date, a.DatoTid) >=  CONVERT(date, getdate())
                                                and a.Afgangs_id > 0
                                               order by c_bookid, b.Booking_id , a.DatoTid", customer_id, company_id);

                Dictionary<string, object> result = new Dictionary<string, object>();

                List<Dictionary<string, object>> user_bookings = GetJsonList(sql, conn);


                List<Dictionary<string, object>> user_bookings_not_ready = new List<Dictionary<string, object>>();


                List <CBooking> user_collection = new List<CBooking>(); 


                int flag = -1;

                CBooking cbooking = new CBooking();

                List<int> departure_ids = new List<int>();

                foreach (Dictionary<string, object> b in user_bookings)
                {
                    int c_bookid = int.Parse(b["c_bookid"].ToString());

                    


                    


                    if (c_bookid == 0)
                    {
                        user_bookings_not_ready.Add(b);

                    } else
                    {
                        if (flag != c_bookid)
                        {
                            cbooking = new CBooking();
                            var test = (int)b["Afgangs_id"];
                            cbooking.departure_ids = new List<int>();
                            cbooking.departure_ids.Add(test);
                            cbooking.rute_id = (int)b["rute_id"];
                            cbooking.duration = int.Parse(b["duration"].ToString());
                            cbooking.unittype_id = int.Parse(b["EnhedsType_id"].ToString());
                            cbooking.unittype_name = b["enhedstype_navn"].ToString();
                            cbooking.start_havn = (string)b["start_havn"];
                            cbooking.slut_havn = (string)b["slut_havn"];
                            cbooking.slut_havn_id = (int)b["slut_havn_id"];
                            cbooking.start_havn_id = (int)b["start_havn_id"];

                            cbooking.AntalBoern = (int)b["AntalBoern"];
                            cbooking.AntPers = (int)b["AntPers"];
                            cbooking.NumberOfItems = (int)b["NumberOfItems"];
                            cbooking.start_harbors = new List<string>();
                            cbooking.start_harbors.Add((string)b["start_havn"]);

                            cbooking.RegId = (string)b["RegId"];
                            cbooking.SystemOfOrigin = (string)b["SystemOfOrigin"];

                            cbooking.departure_time = (DateTime)b["DatoTid"];
                            cbooking.created_at = (DateTime)b["OprettetDatoTid"];

                            user_collection.Add(cbooking);
                            flag = c_bookid;

                            cbooking.c_bookid = c_bookid;
                            cbooking.amount = int.Parse(b["amount"].ToString());

                            cbooking.bookings = new List<Dictionary<string, object>>();
                            cbooking.bookings.Add(b);

                        }
                        else
                        {
                            cbooking.duration += int.Parse(b["duration"].ToString());
                            cbooking.slut_havn = (string)b["slut_havn"];
                            cbooking.slut_havn_id = (int)b["slut_havn_id"];
                            cbooking.start_harbors.Add((string)b["start_havn"]);
                            cbooking.bookings.Add(b);
                            cbooking.departure_ids.Add((int)b["Afgangs_id"]);
                        }
                    }
                }


                user_collection = user_collection.OrderBy(o => o.departure_time).ToList();


                result.Add("UserCollection", user_collection);
                result.Add("user_bookings_not_ready", user_bookings_not_ready);


                //return Ok(GetJsonList(sql, conn));
                return Ok(result);
            }
        }



        [HttpGet]
        public IHttpActionResult GetCompanies()
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"select * from selskab");



                return Ok(GetJsonList(sql, conn));
            }
        }


        [HttpGet]
        public IHttpActionResult GetPossibleDepartures(string str_start_date, string str_end_date, int departure_harbor_id, int arrival_harbor_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string sql = String.Format(@"
                declare
                 @str_date varchar(30) = '{0}'
		            declare
                    @str_end_date varchar(30) = '{1}'
                    declare
                    @selskab_id int = 0
                    declare @departure_harbor_id int = {2}
		            declare @arrival_harbor_id int = {3}

		           SELECT  CAST(a.DatoTid AS DATE) as dt
		        --count(a.Afgangs_id)
                FROM [Afgange] A
	            inner join Overfarter o on o.Overfart_id = A.Overfart_id
                inner join Havne UH on UH.havne_id = O.SlutHavn_Id
		        inner join Rute R on R.Rute_id = O.Rute_Id
		        where
			       	
                    (
						o.Rute_Id	in (select Rute_id from overfarter where StartHavn_Id = @departure_harbor_id and Rute_id in (select  Rute_id from overfarter where SlutHavn_Id = @arrival_harbor_id) )
						
					)
                     
                    and convert(date,a.DatoTid) BETWEEN convert(date,@str_date) AND convert(date,@str_end_date)
				     and
				     o.Rute_Id			                            
					        in (
				            SELECT Rute_Id  FROM Driftsplan dp  WHERE  (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)))
		
		        group by  CAST(a.DatoTid AS DATE)									
                order by CAST(a.DatoTid AS DATE)", str_start_date, str_end_date, departure_harbor_id, arrival_harbor_id);
                // return Ok(GetJsonList(sql, conn));




                SqlCommand command = new SqlCommand(sql, conn);

                SqlDataReader reader = command.ExecuteReader();

                List<DateTime> list = new List<DateTime>();

                while (reader.Read())
                {
                    list.Add((DateTime)reader["dt"]);
                }

                reader.Close();


                return Ok(list);

            }
        }

        // and convert(date, a.DatoTid) BETWEEN convert(date, @str_date) AND convert(date, @str_end_date)

        [HttpGet]
        public IHttpActionResult GetRoutes(string ruter)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                string rute_sql = String.Format(@"SELECT *, 
                     stuff((select distinct ',' + CAST(StartHavn_Id as varchar(10))
                     from overfarter  where Rute_Id = R.Rute_id
                     for xml path('')),1,1,'') harbor_ids
                    FROM Rute R where Rute_id in ({0})", ruter);
                List<Dictionary<string, object>> routes = GetJsonList(rute_sql, conn);

                foreach (dynamic item in routes)
                {
                    string overfart_sql = String.Format(@"
                                      select o.[Navn]
                                      ,o.[Retur_id]      
                                      ,o.[selskabs_id]      
                                      ,o.[StartHavn_Id]
                                      ,o.[SlutHavn_Id]
                                      ,o.[Rute_Id]
                                      ,o.[Bookingkundetype]
                                      ,o.[emergency]
                                      , isnull(o.direction,'') as direction_path
	                                  , DH.Navn as departure_harbor_name
	                                  , DH.Latitude as departure_harbor_lat
	                                  , DH.Longitude as departure_harbor_lng
	                                  , AH.Navn as arrival_harbor_name
	                                  , AH.Latitude as arrival_harbor_lat
	                                  , AH.Longitude as arrival_harbor_lng
                                      
                                        from overfarter o
                                        inner join Havne DH on DH.havne_id = o.StartHavn_Id
                                        inner join Havne AH on AH.havne_id = o.SlutHavn_Id
                                        where o.Rute_Id = ({0})", item["Rute_id"]);
                    item["crossings"] = GetJsonList(overfart_sql, conn);
                }
                return Ok(routes);
            }
        }

        [HttpGet]
        public IHttpActionResult GetHarborsByRute(string ruter)
        {
            var harbourManager = new HarbourManager();
            var harbours = harbourManager.GetHarborsByRute(ruter);

            return Ok(harbours);
        }

        [HttpGet]
        public IHttpActionResult GetPosition()
        {
            try
            {
                string Url = " https://www.marinetraffic.com/en/ais/details/ships/shipid:155487/mmsi:219016906/vessel:FAABORG";
                HtmlWeb web = new HtmlWeb();
                HtmlDocument doc = web.Load(Url);


                //string summary = doc.DocumentNode.SelectNodes("//*[@class=\"details_data_link\"]")[0].InnerText;
                string str_coordinates = doc.DocumentNode.SelectNodes("//*[@class=\"details_data_link\"]")[0].InnerText;

                List<string> names = str_coordinates.Split('/').ToList<string>();

                var result = new { latitude = names[0].Split('&').ToList<string>()[0], longitude = names[1].Split('&').ToList<string>()[0] };

                return Ok(result);
            }
            catch (Exception e)
            {
                return NotFound();
            }

        }




        [HttpGet]
        public IHttpActionResult GetDeparturesByDate(DateTime departure_date, int portal_id, string ruter = "", int selskabs_id = 0)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;


            var settings = GetSettings(portal_id);

            dynamic cykel_statistik_type_id = settings.cykel_statistik_type_id;

            string rute_query = "";
            if (ruter != "")
            {
                rute_query = String.Format("And Rute_id in({0})", ruter);
            }

            string sql = String.Format(@" declare
                        @str_date varchar(30) = '{1}'
                        declare
                        @selskab_id int = {0}

                        SELECT a.Overfartstid, a.Afgangs_id, a.DatoTid, o.Navn, o.Overfart_id, o.Rute_Id, Sh.Navn as starthavn , uh.Navn as sluthavn , a.Fartoj_id
                              , O.StartHavn_Id as starthavn_id
	                          , O.SlutHavn_Id as sluthavn_id
                              , F.Navn as ferry_name
                              , isnull(F.MaxAntalCykler, 1000)  as max_number_of_bikes
                              , A.BookSelvProcent
                              , EnhedsTypeBookSelvPct
                              , PersonBookSelvPct
                              , tonnage
                              , Akt_lukketid_IfBookings
                              , Udsolgt
                              , Locked
                              , Akt_lukketid  
	                          , 

                                F.MaxDakLang as MaxLength, F.MaxDakHLang as MaxHeight, F.MaxEnh as MaxUnitsAmount, A.MaxPers, 
                                          ISNULL(A.Tonnage, F.MaxVagt) as MaxWeight, 
                                          ISNULL((SELECT sum(Vagt) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumWeight, 
                                          ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumPersons,
                                          ISNULL((SELECT sum(AntEnh) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumUnitsAmount,
                                          ISNULL((SELECT sum(Langde) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumLength, 
                                          ISNULL((SELECT sum(AntalVoksne) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumAdults, 
                                          ISNULL((SELECT sum(AntalBoern) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumChildren, 
                                          ISNULL((SELECT sum(Antaloeboer) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumIslanders ,
                                          ISNULL((SELECT sum(NumberOfItems) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumNumberOfItems ,
                                          ISNULL((SELECT sum(bo.NumberOfItems) FROM Bookinger bo
                                                    inner join enhedstype_statestik_type_liste et on et.enhedstype_id = bo.enhedstype_id
                                                    where  Afgangs_id = a.afgangs_id
                                                    and et.statistik_type_id = {2}),0) as SumNumberBikes,
                                          ISNULL((SELECT count(*) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumBookings ,

                             isnull( 
	                         (
		                         select top 1 ia.Afgangs_id From afgange IA
			                        inner join overfarter IO on  io.Overfart_id = IA.Overfart_id
			                        where iO.StartHavn_Id = O.SlutHavn_Id and IA.DatoTid > A.DatoTid  
			                        and convert(date,IA.DatoTid)  = convert(date,@str_date)
			                        and IA.Fartoj_id = a.Fartoj_id
			
			                        order by IA.DatoTid
		                          )
                            , 0) as naeste

                          FROM [Afgange] A
	                        inner join Overfarter o on o.Overfart_id = A.Overfart_id
	                        inner join Havne SH on SH.havne_id = O.StartHavn_Id
                            Inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                            inner join Havne UH on UH.havne_id = O.SlutHavn_Id

                               ", selskabs_id,  departure_date, cykel_statistik_type_id);

            if (selskabs_id > 0)
            {
                sql += String.Format(@"where convert(date,@str_date) = convert(date,a.Datotid)
			                    and a.selskabs_id = @selskab_id
                                {0}
			                    and a.Fartoj_id in (
				                    SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) {0})
                    order by a.DatoTid", rute_query);
            }
            else
            {
                sql += String.Format(@"where convert(date,@str_date) = convert(date,a.Datotid)	
                                        {0}
			                            and a.Fartoj_id in (
				                        SELECT Fartoj_id  FROM Driftsplan dp  WHERE  (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)){0})
                                        order by a.DatoTid", rute_query);
            }

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
                //return Ok(sql.Replace("\r\n", string.Empty).Replace("\t", string.Empty));

            }
        }

        [HttpGet]
        public IHttpActionResult Get(int company_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                // sprogkoder
                string unit_type_language_phrases = "";
                string language_sql = String.Format("SELECT C.code, L.language FROM company_language_codes C inner join language_codes L on L.code = C.code where selskabs_id = {0} order by position", company_id);
                List<Dictionary<string, object>> language_codes = GetJsonList(language_sql, conn);

                // Unittypes
                foreach (dynamic item in language_codes)
                {
                    string code = item["code"];
                    unit_type_language_phrases += String.Format(" , (SELECT Navn FROM EnhTypSprogTekster where Selskabs_id = {0} and SprogKode = '{1}' and Enhedstype_id = E.Enhedstype_id) as {1} ", company_id, code);
                }
                string sql_unit_types = String.Format(@"select *, ISNULL(Navn,'') as name 
                                    	  ,stuff((select ',' + CAST([person_type_id] as varchar(10))
                                           from  person_types_unit_types  where  E.Enhedstype_id = unit_type_id
                                           for xml path('')),1,1,'') person_type_ids {1}
                                           from Enhedstyper E where active = 1 and Selskabs_Id = {0}", company_id, unit_type_language_phrases);

                List<Dictionary<string, object>> unit_types = GetJsonList(sql_unit_types, conn);

                // Ruter todo: 
                string str_routes = "1, 3, 8 , 10, 11";

                // PersonTypes
                string person_type_language_phrases = "";
                foreach (dynamic item in language_codes)
                {
                    string code = item["code"];
                    person_type_language_phrases += String.Format(", (SELECT name FROM person_type_translations where person_type_id = p.id and code = '{0}') as {0} ", code);
                }

                string sql_person_types = String.Format("select * {1} from person_types p where company_id = {0}", company_id, person_type_language_phrases);

                List<Dictionary<string, object>> person_types = GetJsonList(sql_person_types, conn);

                // result
                Dictionary<string, object> result = new Dictionary<string, object>();

                // Harbors
                var harbourManager = new HarbourManager();
                var harbours = harbourManager.GetHarborsByRute(str_routes);


                string rute_sql = String.Format(@"SELECT *, 
                     stuff((select distinct ',' + CAST(StartHavn_Id as varchar(10))
                     from overfarter  where Rute_Id = R.Rute_id
                     for xml path('')),1,1,'') harbor_ids
                    FROM Rute R where Rute_id in ({0})", str_routes);
                List<Dictionary<string, object>> routes = GetJsonList(rute_sql, conn);

                foreach (dynamic item in routes)
                {
                    string overfart_sql = String.Format(@"
                                      select o.[Navn]
                                      ,o.[Retur_id]      
                                      ,o.[selskabs_id]      
                                      ,o.[StartHavn_Id]
                                      ,o.[SlutHavn_Id]
                                      ,o.[Rute_Id]
                                      ,o.[Bookingkundetype]
                                      ,o.[emergency]
	                                  , DH.Navn as departure_harbor_name
	                                  , DH.Latitude as departure_harbor_lat
	                                  , DH.Longitude as departure_harbor_lng
	                                  , AH.Navn as arrival_harbor_name
	                                  , AH.Latitude as arrival_harbor_lat
	                                  , AH.Longitude as arrival_harbor_lng
                                        from overfarter o
                                        inner join Havne DH on DH.havne_id = o.StartHavn_Id
                                        inner join Havne AH on AH.havne_id = o.SlutHavn_Id
                                        where o.Rute_Id = ({0})", item["Rute_id"]);
                    item["crossings"] = GetJsonList(overfart_sql, conn);
                }
                result.Add("unit_types", unit_types);
                result.Add("language_codes", language_codes);
                result.Add("routes", routes);
                result.Add("person_types", person_types);
                result.Add("harbors", harbours);
                return Ok(result);

            }
        }
    }
}
