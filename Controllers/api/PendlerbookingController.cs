﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using OeHopWebPortal.Models;
using System.Globalization;
using Newtonsoft.Json.Linq;
using CompuFerryManager;
using System.Threading.Tasks;
using System.Net;

namespace OeHopWebPortal.Controllers.api
{
    public class PendlerbookingController : BaseApiController
    {

        private readonly BookingManager _groupedBookingManager;

        public PendlerbookingController()
        {
            _groupedBookingManager = new BookingManager();
        }

        [HttpGet]
        public IHttpActionResult GetSiteIdBySiteName(string site_name)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string sql = String.Format(@"select selskabs_id from selskab where Compubook_Siteid = '{0}'", site_name);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                List<Dictionary<string, object>> list = GetJsonList(sql, conn);
                if (list.Count() > 0)
                {
                    return Ok(list[0]);
                }
                else
                {
                    return NotFound();
                }

            }
        }


        [HttpGet]
        public IHttpActionResult GetPersonTypes(int site_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string sql = String.Format(@"SELECT p.*,
                    stuff((select distinct ',' + CAST(unit_type_id as varchar(10))
                     from person_types_unit_types  where person_type_id = p.id
                     for xml path('')),1,1,'') unit_type_ids 
                    FROM person_types p
                    where company_id = {0} order by p.position asc;", site_id);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult GetUnitTypes(int site_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string sql = String.Format(@"select E.*,
                                        stuff((select distinct ',' + CAST(person_type_id as varchar(10))
                                                             from person_types_unit_types  where unit_type_id = E.Enhedstype_id
                                                             for xml path('')),1,1,'') person_type_ids ,
                                        stuff((select distinct ',' + CAST(Fartojs_id as varchar(10))
                                                             from EnhTypFartoj  where Enhedstype_id = E.Enhedstype_id
                                                             for xml path('')),1,1,'') ferry_ids ,
                                        L.statistik_type_id,
                                        isnull(U.json,'') as limits

                                        from Enhedstyper E
                                        left join enhedstype_statestik_type_liste L on l.enhedstype_id = E.Enhedstype_id
                                        left join UnittypeLimits U on U.item_id = E.Enhedstype_id and U.type = 'unit'
                                        where selskabs_id = {0}
                                        and E.Enhedstype_id in (select Enhedstype_id from EnhTypFartoj)
                                        order by E.sortering asc;", site_id);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }


        // GET: api/Pendlerbooking
        [HttpGet]
        public IHttpActionResult GetHarbors(string site_id, string start_date, string end_date, string ruter)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string sql_ruter = String.Format(@" select distinct  D.Fartoj_id as ferry_id, R.*, F.Navn as ferry_name  from rute R
                                                inner join Driftsplan D on d.Rute_id = R.Rute_id
                                                inner join Fartojer F on F.Fartoj_id = D.Fartoj_id
                                                where R.Rute_id in ( {0})
                          --                     and     R.Rute_id in 
                          --                         (
				          --                            SELECT  Rute_id  FROM Driftsplan dp  WHERE  convert(date,'{1}') BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato))



                                                 ", ruter, start_date);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sql_ruter, conn);
                SqlDataReader reader = command.ExecuteReader();

                List<Rute> rute_liste = new List<Rute>();

                while (reader.Read())
                {
                    int Rute_id = (int)reader["rute_id"];
                    string rute_navn = (string)reader["Navn"];
                    Rute rute = new Rute();

                    rute.id = Rute_id;
                    rute.name = rute_navn;
                    rute.ferry_name = (string)reader["ferry_name"];
                    rute.ferry_id = (int)reader["ferry_id"];
                    rute_liste.Add(rute);
                }

                reader.Close();

                foreach (Rute rute in rute_liste)
                {

                    int Rute_id = rute.id;
                    string sql = String.Format(@"declare @site  as int;
                    declare @str_start_date as DateTime;
                    declare @str_end_date as DateTime;
                    set @site   = {0}
                    set @str_start_date = Cast('{1}'as datetime)
                    set @str_end_date = Cast('{2}'as datetime)

                    SELECT H.* FROM Havne H

                    where 
                    H.havne_id in
                    (
	                    select o.StartHavn_Id from afgange A
	                    inner join overfarter o on o.Overfart_id  = A.Overfart_id
	                    where
	                     o.selskabs_id = @site  and convert(date, A.DatoTid)  >= @str_start_date and convert(date, A.DatoTid) <=  @str_end_date
                        and o.rute_id = {3}
	                    and	 O.Rute_id
	                    in (
		                     SELECT Rute_id  FROM Driftsplan dp 
		                     WHERE  (convert(date,@str_start_date)  BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato))
		                     --  and (convert(date,@str_end_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato))
	                       )
                    )", site_id, start_date, end_date, Rute_id);
                    rute.harbors = GetJsonList(sql, conn);
                }
                return Ok(rute_liste);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Book(PendlerBookingModel options)
        {
            //TODO
            // Hvis det går godt returneres redirect url til booking.  asd asdasd asdasdasdsadsdsd
            // Hvis nogle afgange ikke kan bookes returneres disse afgange med afgangs DateTime men også  redirect url til booking. 
            var bookingRequest = new CompuFerryManager.Models.BookingRequestModel();
            var bookingRequestPersonTypes = new List<CompuFerryManager.Models.PersonTypeDto>();
            foreach (var item in options.PersonTypes)
            {
                bookingRequestPersonTypes.Add(new CompuFerryManager.Models.PersonTypeDto()
                {
                    Amount = item.Amount,
                    Id = item.Id
                });
            }
            bookingRequest.PersonTypes = bookingRequestPersonTypes;
            bookingRequest.DepartureIds = options.DepartureIds;
            bookingRequest.PortalId = 0;
            bookingRequest.CustomerName = options.CustomerName;
            bookingRequest.CustomerPhone = options.CustomerPhone;
            bookingRequest.UnitType = new CompuFerryManager.Models.UnitTypeDto()
            {
                Id = options.UnitType.Id,
                Hight = options.Height,
                Length = options.Length,
                NumberOfItems = options.NumberOfItems,
                Weight = options.Weight,
                NumberOfUnits = options.NoOfUnits,
                RegId = options.RegID
            };
            var result = await _groupedBookingManager.BookPendler(bookingRequest, true, true);

            if(result.Success == true)
            {
                return Ok(result);
            } else
            {
                return Content(HttpStatusCode.BadRequest, new { NotBooked = result.NotBooked });
                

            }
            

           // var test_result = 317447;
            //return Content(HttpStatusCode.BadRequest, test_result);
        }


        [HttpPost]
        public IHttpActionResult DepartureAvailability(JObject options)
        {


            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            List<Departure> departures = new List<Departure>();

            dynamic json = options;
            int site_id = json.site_id;
            string start_date = json.str_start_date;
            string end_date = json.str_end_date;
            int route_id = json.RouteId;
            
            int unit_type_id = json.UnitTypeId;
            int number_of_persons = json.NoOfPersons;
            int number_of_children = json.NoOfChildren;
            int number_of_islanders = json.NoOfIslanders;

            int length = json.Length;
            int number_of_units = json.NoOfUnits;
            int number_of_items = json.NumberOfItems;


            int height = json.Height;
            int weight_cargo = json.WeightCargo;
            int weight = json.Weight;
            int weight_total = weight_cargo + weight;
            string str_departure_ids = json.str_departure_ids;


            string sql = "";

            if (length == 0 && number_of_units == 0)
            {
                //ingen kapacitetsberegning
                sql = String.Format(@"
                     select B.*, O.Navn , 

                                ( select

                                                         count(distinct A.Afgangs_id) as amount

                                                        from Afgange A                                                 
                                                        inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                                                        where A.Afgangs_id =  B.Afgangs_id 
                                                        And F.MaxPers - {1} - ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id ),0) > 0

                                ) as number_of_persons_ok 

                     from Afgange B 
                     inner join Overfarter O on B.Overfart_id = O.Overfart_id
                     where B.Afgangs_id   in({0})



                ", str_departure_ids,  number_of_persons);

            }
            else
            {
                sql = String.Format(@"

                   select B.*, O.Navn , 

                (    select

                         count(distinct A.Afgangs_id) as amount

                        from Afgange A 
                        inner join FartojSektioner S on S.Fartoj_id = A.Fartoj_id
                        inner join FartojEnhTypSektioner T on T.Fartojs_id =  A.Fartoj_id and T.Sektion_id = s.Sektion_Id and T.Enhedstype_id = {1}
                        inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                        where A.Afgangs_id  =  B.Afgangs_id
                        and 
                        (
                        S.DaekLaengde - {2} - ISNULL((SELECT sum(Langde * NumberOfItems) FROM Bookinger where  Afgangs_id = a.afgangs_id and Bookinger.BookedOnSection = S.Sektion_Id),0) > 0
                        or 
                        S.MaxAntalEnheder - {3} - ISNULL((SELECT sum(AntEnh * NumberOfItems) FROM Bookinger where  Afgangs_id = a.afgangs_id and Bookinger.BookedOnSection = S.Sektion_Id),0) > 0
                        )
                        AND (F.MaxEnh * EnhedsTypeBookSelvPct / 100)  - {3} - ISNULL(( select sum(NumberOfItems * AntEnh) FROM Bookinger where  Afgangs_id = a.afgangs_id ),0) > 0

               ) as length_ok 

                ,
             (    select

                         count(distinct A.Afgangs_id) as amount

                        from Afgange A 
                        inner join FartojSektioner S on S.Fartoj_id = A.Fartoj_id
                        inner join FartojEnhTypSektioner T on T.Fartojs_id =  A.Fartoj_id and T.Sektion_id = s.Sektion_Id and T.Enhedstype_id = {1}
                        inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                        where A.Afgangs_id =  B.Afgangs_id
                        
                        And (F.MaxVagt * A.BookSelvProcent / 100) - {4} - ISNULL((SELECT sum(NumberOfItems * Vagt) FROM Bookinger where  Afgangs_id = a.afgangs_id ),0) > 0
                        


               ) as weight_ok 

                   ,
             (    select

                         count(distinct A.Afgangs_id) as amount

                        from Afgange A 
                        inner join FartojSektioner S on S.Fartoj_id = A.Fartoj_id
                        inner join FartojEnhTypSektioner T on T.Fartojs_id =  A.Fartoj_id and T.Sektion_id = s.Sektion_Id and T.Enhedstype_id = {1}
                        inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                        where A.Afgangs_id   =  B.Afgangs_id                
                        And (F.MaxPers * A.PersonBookSelvPct / 100) - {5} - ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id ),0) > 0


               ) as number_of_persons_ok 
                	from Afgange B 
                    inner join Overfarter O on B.Overfart_id = O.Overfart_id
                    where B.Afgangs_id   in({0})

                        ", str_departure_ids, unit_type_id, length * number_of_items, number_of_units * number_of_items, weight_total * number_of_items, number_of_persons);

            }

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }

            
        }







        [HttpPost]
        public IHttpActionResult GetDepartures(JObject options)
        {

            dynamic json = options;
            int site_id = json.site_id;
            string start_date = json.str_start_date;
            string end_date = json.str_end_date;
            int route_id = json.RouteId != null ? json.RouteId : 0;
            int departure_harbor_id = json.DepartureHarborId;
            int arrival_harbor_id = json.ArrivalHarborId;
            int unit_type_id = json.UnitTypeId;
            int number_of_persons = json.NoOfPersons;
            int number_of_children = json.NoOfChildren;
            int number_of_islanders = json.NoOfIslanders;

            int length = json.Length;
            int number_of_units = json.NoOfUnits;
            int number_of_items = json.NumberOfItems;


            int height = json.Height;
            int weight_cargo = json.WeightCargo;
            int weight = json.Weight;
            int weight_total = weight_cargo + weight;

            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

            string limit_by_route_sql = "";
            if(route_id > 0 )
            {
                limit_by_route_sql = "And Rute_id = route_id";
            }
            

            string sql = String.Format(@"
                       declare @str_date varchar(30) = '{2}'
                    
                        declare @str_end_date varchar(30) = '{3}'
                        declare @selskab_id int = {0}
                        declare @rute varchar(30) = '{1}'
                       SELECT        convert(varchar(30), convert(date,a.Datotid)) as dato
                                    , a.Overfartstid, a.Afgangs_id, a.DatoTid, o.Navn, o.Overfart_id, o.Rute_Id, Sh.Navn as starthavn , uh.Navn as sluthavn , a.Fartoj_id
	                                , O.StartHavn_Id as starthavn_id
	                                , O.SlutHavn_Id as sluthavn_id
	                                , F.Navn as ferry_name
	                                , A.BookSelvProcent
	                                , EnhedsTypeBookSelvPct
	                                , PersonBookSelvPct
	                                , tonnage
	                                , Akt_lukketid_IfBookings
	                                , Udsolgt
	                                , Locked
	                                , Akt_lukketid  
                                    , Overfartstid
	                                , 
	                                --  F.MaxDakLang as MaxLength, F.MaxDakHLang as MaxHeight, F.MaxEnh as MaxUnitsAmount, A.MaxPers, 
				                    --            ISNULL(A.Tonnage, F.MaxVagt) as MaxWeight, 
				                    --            ISNULL((SELECT sum(Vagt) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumWeight, 
				                    --            ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumPersons,
				                    --            ISNULL((SELECT sum(AntEnh) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumUnitsAmount,
				                    --            ISNULL((SELECT sum(Langde) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumLength, 
				                    --            ISNULL((SELECT sum(AntalVoksne) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumAdults, 
				                    --            ISNULL((SELECT sum(AntalBoern) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumChildren, 
				                    --            ISNULL((SELECT sum(Antaloeboer) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumIslanders ,
				                    --            ISNULL((SELECT count(*) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumBookings ,
                                    isnull( 
	                                (
		                                select top 1 ia.Afgangs_id From afgange IA
		                                inner join overfarter IO on  io.Overfart_id = IA.Overfart_id
		                                where iO.StartHavn_Id = O.SlutHavn_Id and IA.DatoTid > A.DatoTid  
		                                and convert(date,IA.DatoTid)  = convert(date,a.DatoTid)
		                                and IA.Fartoj_id = a.Fartoj_id
			
		                                order by IA.DatoTid
		                                )
                                , 0) as naeste

                                FROM [Afgange] A
                                inner join Overfarter o on o.Overfart_id = A.Overfart_id
                                inner join Havne SH on SH.havne_id = O.StartHavn_Id
                                Inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

                                inner join Havne UH on UH.havne_id = O.SlutHavn_Id

                                where   convert(date,a.Datotid) >= convert(date,@str_date)
	                                and convert(date,a.Datotid) <= convert(date,@str_end_date)	
	                                and a.selskabs_id = @selskab_id
	                                and a.Fartoj_id in (
		                                --SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) And Rute_id = @rute)
		                                SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,a.Datotid) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) {6})
                                order by a.DatoTid
                                                      ", site_id, route_id, start_date, end_date, departure_harbor_id, arrival_harbor_id, limit_by_route_sql);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                AvailabilityManager _availabilityManager = new AvailabilityManager();
                conn.Open();

                List<Departure> departures = new List<Departure>();
                List<Dictionary<string, object>> list = GetJsonList(sql, conn);

                for (int i = 0; i < list.Count; i++)
                {
                    var elm = list[i];
                    int starthavn_id = (int)elm["starthavn_id"];
                    int sluthavn_id = (int)elm["sluthavn_id"];
                    int Afgangs_id = (int)elm["Afgangs_id"];
                    string Navn = (string)elm["Navn"];

                    if (starthavn_id == departure_harbor_id)
                    {
                        Departure departure = new Departure();
                        departure.path = new List<int>();
                        departure.path.Add(Afgangs_id);
                        departure.DatoTid = (DateTime)elm["DatoTid"];
                        departure.Navn = Navn;
                        departure.isGood = true;
                        departure.Afgangs_id = Afgangs_id;
                        departure.naeste = (int)elm["naeste"];
                        departure.dato = (string)elm["dato"];
                        departure.isFirst = false;
                        departure.isLast = false;
                        departure.TravelTime += (int)elm["Overfartstid"];
                        departure.ArrivalTime = departure.DatoTid.AddMinutes(departure.TravelTime);
                        departure.Locked = (bool)elm["Locked"];
                        departure.BookSelvProcent = (int)elm["BookSelvProcent"];
                        departure.PersonBookSelvPct = (int)elm["PersonBookSelvPct"];

                        if (arrival_harbor_id == sluthavn_id)
                        {
                            departures.Add(departure);

                        }
                        else
                        {
                            if ((int)elm["naeste"] != 0)
                            {
                                // Calling recursiv function traversing rest of the departures

                                Departure next_departure = GetNextDeparture(i, i, departure, list, departure_harbor_id, arrival_harbor_id);

                                if (next_departure.isGood == true)
                                {
                                    departures.Add(next_departure);
                                }
                            }
                        }
                    }
                }
                int counter = 0;
                string check = "";
                foreach (Departure d in departures)
                {
                    d.Udsolgt = !_availabilityManager.IsAvaliable(d.path, unit_type_id, conn, number_of_persons, number_of_children, number_of_islanders, length, number_of_units, number_of_items, height, weight_total);


                    if (counter == 0)
                    {
                        check = d.dato;
                        d.isFirst = true;
                    }
                    else
                    {
                        if (check != d.dato)
                        {
                            d.isFirst = true;
                            check = d.dato;
                            departures[counter - 1].isLast = true;
                        }
                    }

                    counter += 1;
                }

                return Ok(departures);
            }
        }





        private Departure GetNextDeparture(int index, int i, Departure departure, List<Dictionary<string, object>> list, int departure_harbor_id, int arrival_harbor_id)
        {
            for (var j = i + 1; j < list.Count; j++)
            {

                if ((int)list[i]["naeste"] == (int)list[j]["Afgangs_id"])
                {
                    if (((int)list[j]["naeste"] == 0) || (int)list[j]["starthavn_id"] == departure_harbor_id)
                    // if ( (int)list[j]["starthavn_id"] == departure_harbor_id)
                    {
                        departure.path = null;
                        departure.isGood = false;
                    }
                    else
                    {
                        departure.path.Add((int)list[j]["Afgangs_id"]);
                        departure.TravelTime += (int)list[j]["Overfartstid"];
                        departure.ArrivalTime = departure.DatoTid.AddMinutes(departure.TravelTime);

                        if ((int)list[j]["sluthavn_id"] != arrival_harbor_id)
                        {
                            departure = GetNextDeparture(index, j, departure, list, departure_harbor_id, arrival_harbor_id);
                        }
                    }
                    return departure;
                }
            }
            return departure;
        }



    }
    public class PendlerBookingPersonType
    {
        public int Id { get; set; }
        public int Amount { get; set; }
    }

    public class PendlerBookingUnitType
    {
        public int Id { get; set; }
    }

    public class PendlerBookingModel
    {
        public int PortalId { get; set; }
        public List<List<int>> DepartureIds { get; set; }
        public List<PendlerBookingPersonType> PersonTypes { get; set; }
        public PendlerBookingUnitType UnitType { get; set; }
        public string ReturnUrl { get; set; }
        public int Height { get; set; }
        public int Length { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChildren { get; set; }
        public int NoOfIslanders { get; set; }
        public int NoOfPersons { get; set; }
        public int NoOfUnits { get; set; }
        public int NumberOfItems { get; set; }
        public int RouteId { get; set; }
        public DateTime StartDate { get; set; }
        public int Weight { get; set; }
        public int WeightCargo { get; set; }
        public int site_id { get; set; }
        public string Comment { get; set; }
        public string RegID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
    }
}