﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CompuFerryManager;
using CSDiverseSettings.Models;
using OeHopWebPortal.Controllers.api;

namespace CSDiverseSettings.Controllers.api
{
    public class PriceController : BaseApiController
    {
        private readonly PriceManager _priceManager;

        public PriceController()
        {
            _priceManager = new PriceManager();
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetPrice(PriceRequestModel priceRequestModel)
        {
            decimal? price = 0;
            var portalId = GetPortalId();
            price = await _priceManager.GetPrice(priceRequestModel.DepartureIds, priceRequestModel.PersonTypes, priceRequestModel.UnitType,
                                                    DateTime.Now, DateTime.Now, portalId);

            return Ok(price);
        }
    }
}