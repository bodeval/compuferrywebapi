﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CompuFerryManager;
using CompuFerryManager.Models;
using System.Collections.Generic;

namespace HjejlenWebPortal.Controllers.api
{
    public class ReceiptController : ApiController
    {
        private readonly IReceiptManager _receiptManger;
        private readonly BookingManager _bookingManager;
        public ReceiptController()
        {
            _receiptManger = new ReceiptManager();
            _bookingManager = new BookingManager();
        }

        [HttpPost]
        public IHttpActionResult SendReceiptAsync(SendReceiptRequest sendRequest)
        {
            _receiptManger.SendReceiptAsync(sendRequest.OrderId, sendRequest.Type, sendRequest.LanguageCode);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult SetBookingsFinalized(SetBookingsFinalizedRequest sendRequest)
        {
            _bookingManager.UpdateBookings(sendRequest.OrderId);
            return Ok();
        }
    }

    public class SendReceiptRequest
    {
        public int OrderId { get; set; }
        public int Type { get; set; }
        public string LanguageCode { get; set; }
    }

    public class SetBookingsFinalizedRequest
    {
        public int OrderId { get; set; }
        public int Type { get; set; }
    }
}