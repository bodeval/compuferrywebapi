﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using CompuFerryManager;
using Serilog;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors;

namespace OeHopWebPortal.Controllers.api
{
    
    public class UnitTypesController : BaseApiController
    {
        private readonly ILogger _log;

        public UnitTypesController()
        {
            _log = Log.ForContext<UnitTypesController>();
        }


        [HttpGet]
        public IHttpActionResult GetHarborsByDate(int portalId, string start_date = "")
        {
            try
            {
                // Routes
                var routeManager = new RouteManager();
                var routes = routeManager.GetRoutes(portalId);

                // Harbors
                var harbourManager = new HarbourManager();
                var harbours = harbourManager.GetHarborsByRute(routes, start_date);
                return Ok(harbours);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Error when trying to get harbors, portalId={portalId}", portalId);
                return InternalServerError(ex);
            }
        }



        [HttpGet]
        public IHttpActionResult Get(int portalId, string start_date = "")
        {
            try
            {
                // Portal
                var portalManager = new PortalManager();
                var portal = portalManager.GetPortal(portalId);

                // Routes
                var routeManager = new RouteManager();
                var routes = routeManager.GetRoutes(portalId);

                // Harbors
                var harbourManager = new HarbourManager();
                var harbours = harbourManager.GetHarborsByRute(routes, start_date);

                // sprogkoder
                var languageCodeManager = new LanguageCodeManager();
                var languageCodes = languageCodeManager.GetLanguagesByCompany(portal.CompanyId);

                // Unittypes
                var unitTypesManager = new UnitTypeManager();
                var unitTypes = unitTypesManager.GetUnitTypes(portal.CompanyId, languageCodes);

                // PersonTypes
                var personTypesManager = new PersonTypeManager();
                var personTypes = personTypesManager.GetPersonTypes(portal.CompanyId, languageCodes);


                var settings = GetSettings(portalId);
                //var settings = GetPortalSettingsFromDB();

                var favorit_tours = GetFavoritTours(portalId);


                // Result
                var result = new Dictionary<string, object> { { "settings", settings }, { "unit_types", unitTypes }, { "language_codes", languageCodes }, { "routes", routes }, { "person_types", personTypes }, { "harbors", harbours }, { "favorit_tours", favorit_tours } };
                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Error when trying to get info, portalId={portalId}", portalId);
                return InternalServerError(ex);
            }
        }





        [HttpGet]
        public IHttpActionResult HjejlenGet(int portalId, string start_date = "")
        {
            try
            {
                // Portal
                var portalManager = new PortalManager();
                var portal = portalManager.GetPortal(portalId);

                // Routes
                var routeManager = new RouteManager();
                var routes = routeManager.GetRoutes(portalId);

                // Harbors
                var harbourManager = new HarbourManager();
                var harbours = harbourManager.GetHarborsByRute(routes, start_date);

                // sprogkoder
                var languageCodeManager = new LanguageCodeManager();
                var languageCodes = languageCodeManager.GetLanguagesByCompany(portal.CompanyId);

                // Unittypes
                var unitTypesManager = new UnitTypeManager();
                var unitTypes = unitTypesManager.GetUnitTypes(portal.CompanyId, languageCodes);

                // PersonTypes
                var personTypesManager = new PersonTypeManager();
                var personTypes = personTypesManager.GetPersonTypes(portal.CompanyId, languageCodes);


                var settings = GetSettings(portalId);
                //var settings = GetPortalSettingsFromDB();

                // 18, 19, 20, 21, 22, 23
                // last departure in database among portal routes

                string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;

                var arr_routes = new List<string>();

                foreach (var item in routes)
                {
                    arr_routes.Add(item.Rute_id.ToString());
                }

                var str_routes = String.Join(",", arr_routes);

                var maxDateSql = String.Format(@"SELECT max(Datotid) as value
                                 FROM Afgange A
                                 inner join overfarter O on O.Overfart_id = A.overfart_id
                                  where o.Rute_Id in ({0} ) and a.afgangs_id > 0", str_routes);

                dynamic maxDate = new JObject();

                DateTime start_datetime = DateTime.Now;

                dynamic possible_dates = GetPossibleDates(start_datetime, portalId);



                using (SqlConnection conn = new SqlConnection(sConn))
                {
                    conn.Open();
                    maxDate = GetJsonList(maxDateSql, conn);

                }

                //        

                // Result
                var result = new Dictionary<string, object> { { "settings", settings }, { "unit_types", unitTypes }, { "language_codes", languageCodes }, { "routes", routes }, { "person_types", personTypes }, { "harbors", harbours }, { "maxDate", maxDate }, { "possible_dates", possible_dates } };
                return Ok(result);
            }
            catch (Exception ex)
            {
                _log.Error(ex, "Error when trying to get info, portalId={portalId}", portalId);
                return InternalServerError(ex);
            }
        }



        internal List<DateTime> GetPossibleDates(DateTime start_datetime, int portal_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                

                conn.Open();
                string sql = String.Format(@"
                declare
                 @str_date varchar(30) = '{0}'
                 
                SELECT  distinct CAST(a.DatoTid AS DATE) as dt
                FROM [Afgange] A
                inner join portals p on p.CompanyId = a.selskabs_id
                where p.id = {1} 
                --and CAST(a.DatoTid AS DATE) >= '{0}'
                and a.DatoTid  >= '{0}'
                order by CAST(a.DatoTid AS DATE)
                ", start_datetime, portal_id);

                SqlCommand command = new SqlCommand(sql, conn);

                SqlDataReader reader = command.ExecuteReader();

                List<DateTime> list = new List<DateTime>();

                while (reader.Read())
                {
                    list.Add((DateTime)reader["dt"]);
                }

                reader.Close();
                return list;
            }
        }
    }
}