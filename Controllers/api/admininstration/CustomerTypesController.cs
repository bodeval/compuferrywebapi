﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace OeHopWebPortal.Controllers.api.admininstration
{
    public class CustomerTypesController : BaseAdministrationController
    {
        [HttpGet]
        public IHttpActionResult GetSites()
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            String sql = "select * from selskab";
                
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult GetCustomerTypes(int portal_id)
        {
            
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            String sql = String.Format("select * from portal_customer_types where portal_id = {0}", portal_id);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult Delete(int id)
        {

            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            String sql = String.Format("delete from portal_customer_types where id= {0}", id);

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }



        [HttpPost]
        public IHttpActionResult Save(JObject incoming)
        {
            
            dynamic json = incoming;

            dynamic id = json.id;

            int portal_id = json.portal_id;
            dynamic company_id = json.company_id;
            dynamic company_customer_type = json.company_customer_type;
            dynamic portal_customer_type = json.portal_customer_type;

            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            try
            {
                using (SqlConnection conn = new SqlConnection(sConn))
            {

                String sql = "";
                conn.Open();

                    if (id == 0)
                    {
                        sql = String.Format(@"INSERT into portal_customer_types (portal_id, company_id, company_customer_type, portal_customer_type)
                                        OUTPUT Inserted.ID
                                        Values({0}, {1}, '{2}', '{3}')", portal_id, company_id, company_customer_type, portal_customer_type);

                        using (SqlCommand command = new SqlCommand(sql, conn))
                        {

                            SqlDataReader reader = command.ExecuteReader();



                            while (reader.Read())
                            {
                                id = (int)reader["ID"];
                            }

                            reader.Close();
                            json.id = id;
                        }

                    }

                    else
                    {
                        sql = String.Format(@"update portal_customer_types set company_id = {0}, company_customer_type = '{1}', portal_customer_type = '{2}', updated_at = GetDate()  where id = {3}", company_id, company_customer_type, portal_customer_type, id);

                        using (SqlCommand command = new SqlCommand(sql, conn))
                        {
                            command.ExecuteNonQuery();
                        }
                    }

                }




                return Ok(json);
            }

            catch(Exception e )
            {
                return Content(HttpStatusCode.NotFound, "Foo does not exist.");
            }
                

        }
    }
}
