﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OeHopWebPortal.Controllers.api.admininstration
{
    public class PortalUnitTypeMappingController : BaseAdministrationController
    {

        [HttpGet]
        public IHttpActionResult GetPortalCompanies(int portal_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                conn.Open();


                // companies / sites
                string sql = String.Format(@"
                declare @portal_id int = {0}     
                SELECT distinct r.selskabs_id, s.Navn
                FROM [Rute] R
                inner join Selskab s on s.Selskabs_id = r.Selskabs_Id
                where r.Rute_id in (select RouteId from PortalRoute where PortalId = @portal_id)", portal_id);
                List<Dictionary<string, object>> companies = GetJsonList(sql, conn);

                foreach (Dictionary<string, object> item in companies)
                {
                    item.Add("unit_types", GetJsonList(String.Format(@"
                            select * from Enhedstyper where selskabs_id = {0}", item["selskabs_id"]), conn));

                    item.Add("person_types", GetJsonList(String.Format(@"
                            select * from person_types where company_id = {0}", item["selskabs_id"]), conn));

                    item.Add("unit_type_relations", GetJsonList(String.Format(@"
                            select SiteUnittypeId, PortalUnitTypeId from PortalUnittypeRelation where company_id = {0} and portal_id = {1} ", item["selskabs_id"], portal_id), conn));

                    item.Add("person_type_relations", GetJsonList(String.Format(@"
                            select SitePersonTypeId, PortalPersonTypeId from PortalPersonTypeRelation where company_id = {0} and portal_id = {1} ", item["selskabs_id"], portal_id), conn));



                }

                result.Add("sites", companies);

                // unit_types
                result.Add("portal_unit_types", GetJsonList(String.Format(@"
                            SELECT T.*
                            FROM [Enhedstyper] t
                            inner join portals p on p.CompanyId = t.selskabs_id
                            where p.Id =  {0}", portal_id), conn));
                // person_types
                result.Add("portal_person_types", GetJsonList(String.Format(@"
                            SELECT T.*
                            FROM [person_types] t
                            inner join portals p on p.CompanyId = t.company_id
                            where p.Id = {0}", portal_id), conn));
                return Ok(result);
            }
        }



        [HttpPost]
        public IHttpActionResult Save(JObject incoming)
        {
            
            dynamic json = incoming;

            int portal_id = json.portal_id;

            int company_id = json.site.selskabs_id;

            dynamic person_types = json.site.portal_unit_types;
            
            dynamic portal_person_types = json.site.portal_person_types;
            dynamic portal_unit_types = json.site.portal_unit_types;


            string sql_insert = "";
            string sql_delete = String.Format(@"delete from PortalPersontypeRelation where company_id = {0} and  portal_id = {1};", company_id, portal_id);
            sql_delete += String.Format(@"delete from PortalUnittypeRelation where company_id = {0} and  portal_id = {1};", company_id, portal_id);

            foreach (dynamic person_type in portal_person_types)
            {
                sql_insert += String.Format(@"insert into PortalPersontypeRelation ( PortalPersontypeId, SitePersontypeId, company_id, portal_id) values( {0}, {1}, {2}, {3});",
                                    person_type.id, person_type.site_person_type_id, company_id, portal_id);
            }


            foreach (dynamic unit_type in portal_unit_types)
            {
                sql_insert += String.Format(@"insert into PortalUnittypeRelation ( PortalUnittypeId, SiteUnittypeId, company_id, portal_id) values( {0}, {1}, {2}, {3});",
                                    unit_type.Enhedstype_id, unit_type.site_unit_type_id, company_id, portal_id);
            }



            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            
                using (SqlConnection conn = new SqlConnection(sConn))
                {

                    conn.Open();
                    SqlTransaction transaction;
                    SqlCommand command = conn.CreateCommand();
                    transaction = conn.BeginTransaction("SampleTransaction");
                    command.Transaction = transaction;
                    command.CommandText = sql_delete + sql_insert;

                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    return Ok(json);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return NotFound();
                }

                
                    

                }
               
            


        }
    }
}
