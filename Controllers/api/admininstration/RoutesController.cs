﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OeHopWebPortal.Controllers.api.admininstration
{
    public class RoutesController : BaseAdministrationController
    {

        [HttpGet]
        public IHttpActionResult Get()
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            String sql = @" SELECT r.* ,s.Navn as SelskabsNavn	  
                FROM Rute R
                inner join selskab s on s.Selskabs_id = r.Selskabs_Id
                order by r.selskabs_id
                ";
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult GetPortalRoutes(int portal_id)

        {
            
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            String sql = String.Format(@" select RouteId as id from PortalRoute where PortalId = {0}", portal_id);
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                return Ok(GetJsonList(sql, conn));
            }
        }



        [HttpPost]
        public IHttpActionResult SaveRoutes(JObject incoming)

        {
            dynamic json = incoming;
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            int portal_id = json.portal_id;


            String sql_delete = String.Format(@" delete from PortalRoute where PortalId = {0};", portal_id);

            string sql_insert = "";

            foreach(dynamic item in json["routes"])
            {
                sql_insert += String.Format("INSERT INTO PortalRoute (PortalId, RouteId) values ({0}, {1});", portal_id, item);
            }

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();
                SqlTransaction transaction;
                SqlCommand command = conn.CreateCommand();
                transaction = conn.BeginTransaction("SampleTransaction");
                command.Transaction = transaction;
                command.CommandText = sql_delete + sql_insert;

                
                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    return Ok(json);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return NotFound();
                }
            }
        
        }

    }
}
