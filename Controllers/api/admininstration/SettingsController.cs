﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OeHopWebPortal.Controllers.api.admininstration
{
    public class SettingsController : BaseAdministrationController
    {

        [HttpGet]
        public IHttpActionResult GetPortals()
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                string sql = "select * from  Portals";
                return Ok(GetJsonList(sql, conn));
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int portal_id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            

                string sql = String.Format("select top 1 * from portalSettings where portal_id = {0} order by id desc", portal_id);
                JObject result = new JObject();
                using (SqlConnection conn = new SqlConnection(sConn))
                {
                    conn.Open();
                
                    SqlCommand command = new SqlCommand(sql, conn);

                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                    result = JObject.Parse((string)reader["content"]);
                    //return Ok((string)reader["content"]);
                }

                    reader.Close();

                    return Ok(result);
                }

          //  GetJsonList(rute_sql, conn);



        }


        [HttpPost]
        public IHttpActionResult Save(dynamic incoming)
        {
            string sConn = ConfigurationManager.ConnectionStrings["FerryDbConnection"].ConnectionString;
            int portal_id = incoming.portal_id;


            dynamic user_settings = incoming.user_settings;
            dynamic system_settings = incoming.system_settings;

            string sql_update = String.Format("insert into PortalSettings(portal_id, content, settings ) values({0}, '{{''settings'': {2} }}', '{1}') ", portal_id, user_settings, system_settings);
            
            using (SqlConnection conn = new SqlConnection(sConn))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sql_update, conn);
                var rows = command.ExecuteNonQuery();

                return Ok(rows);

            }

            



        }


    }
}
