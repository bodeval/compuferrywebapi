﻿using System;
using System.Collections.Generic;

namespace OeHopWebPortal.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public string language { get; set; }
        public int site_id { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
        
        public bool EmailConfirmed { get; set; }

        public string language { get; set; }

        public int site_id { get; set; }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public string Phonenumber { get; set; }

        public dynamic Roles { get; set; }

        public string language { get; set; }

        public int site_id { get; set; }

        public string Id { get; set; }

       

    }
}
