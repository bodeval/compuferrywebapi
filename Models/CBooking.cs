﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OeHopWebPortal.Models
{
    public class CBooking
    {
        public int c_bookid { get; set; }

        public int duration { get; set; }

        public int start_havn_id { get; set; }
        public int rute_id { get; set; }

        public int slut_havn_id { get; set; }

        public int AntPers { get; set; }

        public int AntalBoern { get; set; }

        public int NumberOfItems { get; set; }

        public List<string> start_harbors { get; set; }

        public string slut_havn { get; set; }

        public string enhedstype_navn { get; set; }
        public string RegId { get; set; }
        public string SystemOfOrigin { get; set; }


        public string start_havn { get; set; }
        public string unittype_name { get; set; }

        public DateTime departure_time { get; set; }
        public DateTime arrival_time { get; set; }

        public int amount{ get; set; }

        public int unittype_id{ get; set; }

        public List<Dictionary<string, object>> bookings { get; set; }

        public DateTime created_at { get; set; }

        public List<int> departure_ids{ get; set; }
    }
}