﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OeHopWebPortal.Models
{
    public class Departure
    {
        public int Overfartstid { get; set; }
        public int Afgangs_id { get; set; }
        public DateTime DatoTid { get; set; }
        public string Navn { get; set; }
        public int Overfart_id { get; set; }
        public int Rute_Id { get; set; }
        public string starthavn { get; set; }
        public string sluthavn { get; set; }
        public int Fartoj_id { get; set; }
        public int starthavn_id { get; set; }
        public int sluthavn_id { get; set; }
        public string ferry_name { get; set; }
        public int BookSelvProcent { get; set; }
        public int EnhedsTypeBookSelvPct { get; set; }
        public int PersonBookSelvPct { get; set; }
        public object tonnage { get; set; }
        public object Akt_lukketid_IfBookings { get; set; }
        public bool Udsolgt { get; set; }
        public bool Locked { get; set; }
        public int Akt_lukketid { get; set; }
        public int MaxLength { get; set; }
        public int MaxHeight { get; set; }
        public int MaxUnitsAmount { get; set; }
        public int MaxPers { get; set; }
        public int MaxWeight { get; set; }
        public double SumWeight { get; set; }
        public int SumPersons { get; set; }
        public int SumUnitsAmount { get; set; }
        public double SumLength { get; set; }
        public int SumAdults { get; set; }
        public int SumChildren { get; set; }
        public int SumIslanders { get; set; }
        public int SumBookings { get; set; }
        public int naeste { get; set; }
        public List<int> path { get; set; }
        public bool isGood { get; set; }

        public bool isLast { get; set; }
        public bool isFirst { get; set; }

        public string dato { get; set; }

        public DateTime ArrivalTime { get; set; }
        public int TravelTime { get; set; }
    }
}