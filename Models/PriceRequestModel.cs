﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CompuFerryManager.Models;

namespace CSDiverseSettings.Models
{
    public class PriceRequestModel
    {
        public List<List<int>> DepartureIds { get; set; }
        public List<PersonTypeDto> PersonTypes { get; set; }
        public UnitTypeDto UnitType { get; set; }
    }
}