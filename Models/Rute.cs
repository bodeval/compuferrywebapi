﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace OeHopWebPortal.Models
{
    public class Rute
    {
        public int id { get; set; }
        public string name { get; set; }
        public string ferry_name { get; set; }
        public int ferry_id { get; set; }

        public List<Dictionary<string, object>> harbors { get; set; }
    }
}