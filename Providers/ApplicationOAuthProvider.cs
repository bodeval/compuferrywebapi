﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using OeHopWebPortal.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Http.Cors;

namespace OeHopWebPortal.Providers
{
    
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(user.UserName, user.Id);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string Id)
        {
            var user = GetUserInfo(Id);
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "language", user.language },
                { "Id", Id },
                { "roles", user.Roles }
                
              

            };
            return new AuthenticationProperties(data);
        }

        public static UserLoginInfoViewModel GetUserInfo(string Id)
        {
            string sConn = ConfigurationManager.ConnectionStrings["AdminConnection"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(sConn))
            {
                String sql = String.Format(@"SELECT U.*, IsNull(U.site_id,0) as sid,
                                stuff((select ',' + CAST([Roleid] as varchar(10)) 
                                    from  AspNetUserRoles R  where  U.Id= Userid 
	                                for xml path('')),1,1,'') roles
                                FROM AspNetUsers U                                
                                 where U.Id = '{0}'
                                ", Id);
                UserLoginInfoViewModel result = new UserLoginInfoViewModel();

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    SqlDataReader reader = command.ExecuteReader();
                    
                    if (reader.Read())
                    {
                        result.Roles = (string)reader["roles"];
                        result.language = (string)reader["language"];
                       
                        
                    }
                    conn.Close();
                }
                return result;
            }

        }
    }
}