﻿using System;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.Optimization;
using System.Collections.Generic;

public class Fingerprint
{
    public static string Tag(string rootRelativePath)
    {
        if (HttpRuntime.Cache[rootRelativePath] == null)
        {
            string absolute = HostingEnvironment.MapPath("~" + rootRelativePath);

            DateTime date = File.GetLastWriteTime(absolute);
            int index = rootRelativePath.LastIndexOf('/');

            string result = rootRelativePath.Insert(index, "/v-" + date.Ticks);
            HttpRuntime.Cache.Insert(rootRelativePath, result, new CacheDependency(absolute));
        }

        return HttpRuntime.Cache[rootRelativePath] as string;
    }

    public static class StaticFile
    {
        
        public static string AddTag(string rootRelativePath)
        {
            if (HttpRuntime.Cache[rootRelativePath] == null)
            {
                var absolutePath = HostingEnvironment.MapPath(rootRelativePath);
                var lastChangedDateTime = File.GetLastWriteTime(absolutePath);

                if (rootRelativePath.StartsWith("~"))
                {
                    rootRelativePath = rootRelativePath.Substring(1);
                }
                
                //var versionedUrl = rootRelativePath + "?v=" + lastChangedDateTime.Ticks;
                //var versionedUrl = rootRelativePath + "/v-" + lastChangedDateTime.Ticks;


                int index = rootRelativePath.LastIndexOf('/');

                string versionedUrl = rootRelativePath.Insert(index, "/v-" + lastChangedDateTime.Ticks);

                HttpRuntime.Cache.Insert(rootRelativePath, versionedUrl, new CacheDependency(absolutePath));
            }

            return HttpRuntime.Cache[rootRelativePath] as string;
        }

        
        public static string Version(string bundle_str)
        {
            //List<string> list = new List<string>();
            string list = "";
            

            if (BundleResolver.Current.IsBundleVirtualPath(bundle_str))
            {
                if (!BundleTable.EnableOptimizations)
                {
                    foreach (var path in BundleResolver.Current.GetBundleContents(bundle_str))
                    {
                        
                        //list.Add("<script type=\"text/javascript\"" + Tag(path) + "</script>\n");
                        list += "<script type=\"text/javascript\" src=\"" + AddTag(path) + "\"></script>\n";
                       
                    }
                }
                else
                    list += BundleResolver.Current.GetBundleUrl(bundle_str);
                    
            }
            else
                list += bundle_str;
                
            return list;
            
        }

       
    }
}