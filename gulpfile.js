﻿
"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"),
	concat = require("gulp-concat"),
	cssmin = require("gulp-cssmin"),
	rename = require("gulp-rename"),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
    angularTemplateCache = require('gulp-angular-templatecache'),
    livereload = require('gulp-livereload'),
    cacheBuster = require('gulp-cache-bust');

var paths = {
    sass: "Content/scss/",
    jsOutputDirectory: "Scripts/",
    cssOutputDirectory: "Styles/",
    site_style: "Content/css/"
};

gulp.task("sass:compile", function () {
    return gulp.src(paths.sass + "main.scss")
        .pipe(sass())
        .pipe(rename({ suffix: ".min" }))
		.pipe(cssmin())
        .pipe(cacheBuster())
        .pipe(gulp.dest(paths.cssOutputDirectory));
        
});

gulp.task("bundle:css", ["sass:compile"], function () {
    return gulp.src(paths.site_style + "style.css")
		.pipe(concat("styles.css"))
		.pipe(gulp.dest(paths.cssOutputDirectory))
		.pipe(rename({ suffix: ".min" }))
		.pipe(cssmin())
        .pipe(cacheBuster())
		.pipe(gulp.dest(paths.cssOutputDirectory));
        
});

gulp.task('templates', function () {
    return gulp.src('content/Angular/**/*.html')
        .pipe(htmlmin())
        .pipe(angularTemplateCache('templates.js', {
            module: 'myApp',
            root: '/Content/Angular'
        }))
        .pipe(cacheBuster())
        .pipe(gulp.dest(paths.jsOutputDirectory))
        .pipe(livereload());
});

gulp.task('angular', function () {
       gulp.src(['content/Angular/**/*.js'])
      .pipe(concat('app.js'))
      .pipe(uglify())
    

       .pipe(cacheBuster())
      .pipe(gulp.dest(paths.jsOutputDirectory))
      .pipe(livereload());
})

gulp.task("watch", function () {
    
    livereload.listen();
    
    
    gulp.watch(paths.sass + "**/*.scss", ["sass:compile","bundle:css"]);
    gulp.watch("content/Angular/**/*.js", ["angular"]);
    gulp.watch("content/Angular/**/*.html", ["templates"]);
});

gulp.task('default', ['bundle:css', 'templates', 'angular', 'sass:compile']);